Reference :
  - Create Credential : https://cloud.google.com/docs/authentication/getting-started
  - Django - Cloud SQL : https://cloud.google.com/python/django/appengine
                         https://cloud.google.com/appengine/docs/flexible/python/using-cloud-sql
  - Storge File and Image : https://django-storages.readthedocs.io/en/latest/backends/gcloud.html
  - Reset Migrations on local : https://simpleisbetterthancomplex.com/tutorial/2016/07/26/how-to-reset-migrations.html
Command :
  - Deploy :
    >> gcloud app deploy --project cg-webook --version uno
  - Use Credential :
    >> export GOOGLE_APPLICATION_CREDENTIALS=/home/chetchaiyan/workspace_appengine/try-python3/google_app_credential.json
  - Use Credential in settings.py :
    >> from google.oauth2 import service_account
    >> GS_CREDENTIALS = service_account.Credentials.from_service_account_file( "cg-webook-credential.json" )
  - Download and config cloud sql proxy :
    >> wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
    >> chmod +x cloud_sql_proxy
  - Open cloud sql Proxy :
    >> ./cloud_sql_proxy -instances="cg-webook:asia-east2:cg-webook"=tcp:3306
  - connect to local mysql :
    >> mysql -u db_user -h 127.0.0.1 -p
    >> password : random_password
  - set PATH for gecko
    >> export PATH=$PATH:/home/chetchaiyan

Note
    - Forget password เมื่อกรอกข้อมูลแล้วให้ส่ง Password ใหม่ไปที่ Email Address ได้เลยหรือไม่ หรือต้องมี Interface ในการเปลี่ยน Password
    - Private Message เมื่อคนนึงลบ อีกคนจะหายไปด้วย ถูกต้องหรือไม่

Big Issue
    - ข้อมูลสำหรับจัดส่งสินค้า ทั้งผู้ส่งและผู้รับ เพื่อกรอกข้อมูลให้ตรงกับข้อกำหนดของ ThailandPost
    - ตรวจสอบข้อมูล ชื่อ ที่อยู่ เบอร์โทรศัพท์ Email ให้เรียบร้อยก่อน อนุญาตให้โพสหนังสือ / ซื้อหนังสือ

Debug log File
  - tail -f debug.log

Error log File
  - cat error.log

Host config
    ip: 45.64.186.182
    root_pass: ZjwDv69G8;@Lx5
    user: codegears
    pass: c0deGear$
    project_path: /home/codegears/cg_webook
    project_url: http://45.64.186.182
    database:
    db_name: cg_webook
    db_username: root
    db_password: aiCv72%xOoBQ
    superadmin:
    username: admin
    password: 63AAE3zb@WyU

Manual deploy
    - ssh codegears@45.64.186.182
    - cd /home/codegears/cg_webook && bash update-app.bash