# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import urllib, base64
import logging
import requests

class Mailgun(object):

    def __init__(self, api_key, domain_name, sender):
        self.api_key = api_key
        self.domain_name = domain_name
        self.sender = sender

    def send_message(self, recipient, **kwds):
        url = "https://api.mailgun.net/v3/{}/messages".format(self.domain_name)
        data = {
            'to': recipient,
            'from' : self.sender,
        }
        files = None
        if 'attachment' in kwds :
            files = {'attachment': kwds.pop('attachment')}
        for k in kwds :
            data[k] = kwds[k]
        result = requests.post(url, auth=("api", self.api_key), data=data, files=files)
