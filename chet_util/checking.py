def checking(func):
    check_result = func()
    if check_result :
        return "OK"
    else :
        return "FAIL"
