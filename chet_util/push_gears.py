import requests
import json

# Product Key
# APP_ID = 'agxzfnB1c2gtZ2VhcnNyGAsSC0FwcGxpY2F0aW9uGICAgKzSoJUKDA'
# APP_SECRET = '3rd1ut2877bx8bb0'

# Development Key
APP_ID = 'agxzfnB1c2gtZ2VhcnNyGAsSC0FwcGxpY2F0aW9uGICAgOzC9Y0KDA'
APP_SECRET = '8r8wqnogpulc280h'

'''
5. เมื่อสินค้าถูกจัดส่ง => (order_id, timestamp)

1. เมื่อโพสข้อความ แบบ Announcement จะมีแจ้งเตือนไปถึงทุกคน => (post_id, post_title, post_image, fullName, timestamp)
2. เมื่อมีผู้ใช้อื่น Direct message มาถึงเรา => (user_id, fullName, timestamp) : DONE
3. เมื่อมีการผู้ใช้อื่นคอมเม้นหรือถูกใจโพสของเรา => (post_id, fullName, timestamp) : DONE
4. เมื่อมีคำขอเป็นเพื่อนมาถึงเรา => (user_id, fullName, timestamp) : DONE
6. เมื่อมีผู้ใช้คนอื่นมาถูกใจหนังสือที่วางขาย => (user_id, fullName, timestamp) : DONE
'''

# TEST with user_id 35
def push_all(message, data={}):
    d = {
        'app_id' : APP_ID,
        'app_secret' : APP_SECRET,
        'message' : message,
        'data' : json.dumps(data),
    }
    result = requests.post('https://push-gears.appspot.com/api/push_all', data=d)

def push_target(user_id, message, data={}, badge_count=True):
    d = {
        'app_id' : APP_ID,
        'app_secret' : APP_SECRET,
        'user_id' : user_id,
        'message' : message,
        'data' : json.dumps(data),
        'badge_count' : badge_count,
    }
    result = requests.post('https://push-gears.appspot.com/api/push_target', data=d)
