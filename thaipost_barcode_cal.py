import sys

def main(argv):
    barcode = argv[1]
    digit = barcode[2:10]
    weight = "86423597"
    sum = 0
    for i, w in zip(digit, weight) :
        sum += int(i) * int(w)
    remainder = sum % 11
    if remainder == 0 :
        return "5"
    elif remainder == 1 :
        return "0"
    else :
        return str(11 - remainder)

if __name__ == '__main__' :
    result = main(sys.argv)
    print(result)
