#!/bin/bash
git pull
source cg_webook_env/bin/activate
pip install -r requirements.txt
yes yes | python3 ~/cg_webook/manage.py migrate
deactivate
sudo systemctl daemon-reload
sudo systemctl restart gunicorn