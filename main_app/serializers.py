from django.contrib.auth.models import User, Group
from django.db.models import Q
from rest_framework import serializers
from rest_framework import filters
from .models import *
from django.contrib.auth import get_user_model


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'email')

class UserBonusSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserBonus
        fields = ('create_dt', 'bonus_type', 'bonus_value', 'expire_date', 'usable_constrain', 'usable_remain')

class WeBookUserSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)
    user_bonus = UserBonusSerializer(many=True, read_only=True)

    class Meta:
        model = WeBookUser
        fields = ('user', 'fullname', 'gender', 'birthday', 'education', 'country_code', 'phone_number',
                    'mood_status', 'headline_status', 'address', 'photo', 'con_allow_friend_request',
                    'con_allow_other_to_see_friend', 'recommend', 'level', 'rating', 'zipcode',
                    'province', 'country', 'district', 'rating_display', 'social_fb_id', 'social_fb_token',
                    'social_line_id', 'social_line_token', 'social_google_id', 'social_google_token',
                    'con_p_noti_direct_message', 'con_p_noti_friend_request', 'con_p_noti_post_like_comment',
                    'con_p_noti_announcement', 'con_p_noti_ship', 'con_p_noti_save_book',
                    'con_ia_noti_direct_message', 'con_ia_noti_friend_request', 'con_ia_noti_post_like_comment',
                    'user_bonus', 'announcement_credit', 'announcement_donate_credit', 'coin', 'paypal_email',
                    'current_lat', 'current_long')

        read_only_fields = ('recommend', 'level', 'rating', 'rating_display', 'announcement_credit',
                    'announcement_donate_credit', 'coin')

class UserRatingSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField( default=serializers.CurrentUserDefault() )
    target = serializers.PrimaryKeyRelatedField(queryset=get_user_model().objects.all(), many=False, read_only=False, required=True)

    class Meta:
        model = UserRating
        fields = ('id', 'user', 'target', 'create_dt', 'rating')
        read_only_fields = ('create_dt', )

class PrivateMessageSerializer(serializers.ModelSerializer):
    sender = UserSerializer(many=False, read_only=True)
    receiver = UserSerializer(many=False, read_only=True)
    class Meta:
        model = PrivateMessage
        fields = ('id', 'sender', 'receiver', 'message', 'create_dt', 'newest', 'sender_name', 'receiver_name')
        create_only_fields = ('book', 'sender', 'receiver', 'newest')
        read_only_fields = ('sender_name', 'receiver_name')

class ImagePoolSerializer(serializers.ModelSerializer):
    book = serializers.PrimaryKeyRelatedField(queryset=Book.objects.all(), many=False, read_only=False, required=False)
    report = serializers.PrimaryKeyRelatedField(queryset=Report.objects.all(), many=False, read_only=False, required=False)

    class Meta:
        model = ImagePool
        fields = ('id', 'book', 'image', 'report', 'object_class', 'object_id')
        read_only_fields = ('object_class', 'object_id')

    def create(self, validated_data):
        report = validated_data.pop('report', None)
        book = validated_data.pop('book', None)
        image_pool = ImagePool(**validated_data)
        if report :
            image_pool.object = report
        elif book :
            image_pool.object = book
        image_pool.save()
        return image_pool

class ShippingProviderSerializer(serializers.ModelSerializer):

    class Meta :
        model = ShippingProvider
        fields = ('id', 'name', )

class BookCategorySerializer(serializers.ModelSerializer):

    class Meta :
        model = BookCategory3
        fields = ('id', 'name')

class BookSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField( default=serializers.CurrentUserDefault() )
    image = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    webook_user = WeBookUserSerializer(many=False, read_only=True)
    category = serializers.SlugRelatedField(queryset=BookCategory3.objects.all(),
                many=False, read_only=False, slug_field='name' )
    shipping_provider = serializers.SlugRelatedField(queryset=ShippingProvider.objects.all(),
                many=True, read_only=False, slug_field='name')

    class Meta:
        model = Book
        fields = ('id', 'user', 'title', 'condition', 'category', 'author', 'publication_year', 'book_format',
                'language', 'geo_lat', 'geo_long', 'weight', 'owner_comment', 'rating', 'create_dt', 'user_id',
                'image', 'webook_user', 'review_count', 'shipping_provider', 'price')
        read_only_fields = ('rating', 'image', 'webook_user', 'review_count')

class BookReviewSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField( default=serializers.CurrentUserDefault() )
    book = serializers.PrimaryKeyRelatedField(queryset=Book.objects.all(), many=False, read_only=False, required=True)
    webook_user = WeBookUserSerializer(many=False, read_only=True)

    class Meta:
        model = BookReview
        fields = ('user', 'book', 'review_text', 'rating', 'user_id', 'create_dt', 'webook_user')
        create_only_fields = ('book', )
        read_only_fields = ('create_dt', 'webook_user')

class PostSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField( default=serializers.CurrentUserDefault() )
    webook_user = WeBookUserSerializer(many=False, read_only=True)
    is_user_like = serializers.SerializerMethodField()
    is_user_friend = serializers.SerializerMethodField()
    is_user_noti = serializers.SerializerMethodField()
    is_user_donate = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ('id', 'user', 'post_text', 'post_exposure', 'link', 'create_dt', 'user_id', 'title',
                    'image', 'comment_count', 'like', 'webook_user', 'is_user_like', 'is_user_friend',
                    'is_user_noti', 'is_user_donate', 'is_active', 'geo_lat', 'geo_long', 'is_geo_constrain')
        read_only_fields = ('webook_user', 'is_user_like', 'is_user_friend', 'is_user_noti', 'is_user_donate' )

    def get_is_user_like(self, obj):
        post_like = obj.postlike_set.all().filter(user_id=self.context['request'].user)
        return post_like[0].id if post_like else 0

    def get_is_user_friend(self, obj):
        webook_user_1 = WeBookUser.objects.get(pk=obj.user)
        webook_user_2 = WeBookUser.objects.get(pk=self.context['request'].user)
        return True if webook_user_1 in webook_user_2.friend.all() else False

    def get_is_user_noti(self, obj):
        post_noti = obj.postnotification_set.all().filter(user_id=self.context['request'].user)
        return post_noti[0].id if post_noti else 0


    def get_is_user_donate(self, obj):
        webook_user = WeBookUser.objects.get(pk=obj.user)
        post_donate = obj.postdonate_set.all().filter(user_id=self.context['request'].user)
        return True if post_donate else False

class PostCommentSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField( default=serializers.CurrentUserDefault() )
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all(), many=False, read_only=False, required=True)
    webook_user = WeBookUserSerializer(many=False, read_only=True)

    class Meta:
        model = PostComment
        fields = ('id', 'user', 'post', 'comment_text', 'create_dt', 'user_id', 'webook_user')
        create_only_fields = ('create_id', )
        read_only_fields = ('webook_user', )

class PostLikeSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField( default=serializers.CurrentUserDefault() )
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all(), many=False, read_only=False, required=True)

    class Meta:
        model = PostLike
        fields = ('id', 'user', 'post', 'user_id')

class PostNotificationSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField( default=serializers.CurrentUserDefault() )
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all(), many=False, read_only=False, required=True)

    class Meta:
        model = PostNotification
        fields = ('id', 'user', 'post', 'user_id')

class ReportContentRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        if isinstance(value, Book):
            serializer = BookSerializer(value, context=self.context)
        elif isinstance(value, Post):
            serializer = PostSerializer(value, context=self.context)
        else :
            raise Exception('Unexpected type of tagged object')
        return serializer.data

class ReportSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField( default=serializers.CurrentUserDefault() )
    object = ReportContentRelatedField(read_only=True)
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all(), many=False, read_only=False, required=False)
    book = serializers.PrimaryKeyRelatedField(queryset=Book.objects.all(), many=False, read_only=False, required=False)
    image = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta :
        model = Report
        fields = ('id', 'user', 'report_text', 'create_dt', 'object', 'post', 'book', 'object_class', 'image')
        read_only_fields = ('object_class', 'create_dt', 'image')

    def create(self, validated_data):
        post = validated_data.pop('post', None)
        book = validated_data.pop('book', None)
        report = Report(**validated_data)
        if post :
            report.object = post
        elif book :
            report.object = book
        report.save()
        return report

class PurchaseOrderItemSerializer(serializers.ModelSerializer):
    item = serializers.PrimaryKeyRelatedField(queryset=Book.objects.all(), many=False,
                read_only=False, required=True)
    shipping_provider = serializers.SlugRelatedField(queryset=ShippingProvider.objects.all(),
                many=False, read_only=False, slug_field='name')
    book_detail = BookSerializer(many=False, read_only=True)
    purchase_order = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = PurchaseOrderItem
        fields = ('id', 'item', 'price', 'shipping_provider', 'book_detail', 'is_rated', 'tracking_code',
                    'create_dt', 'purchase_order')
        read_only_fields = ('tracking_code', )

class PurchaseOrderSerializer(serializers.ModelSerializer):
    po_item = PurchaseOrderItemSerializer(many=True,
                read_only=False, required=True)
    user = serializers.HiddenField( default=serializers.CurrentUserDefault() )
    webook_user = WeBookUserSerializer(many=False, read_only=True)

    class Meta:
        model = PurchaseOrder
        fields = ('id', 'po_item', 'user', 'webook_user', 'create_dt', 'payment', 'status',
                    'shipping_address', 'shipping_zipcode', 'shipping_country', 'shipping_province',
                    'shipping_district')
