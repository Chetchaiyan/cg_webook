from django.urls import include, path
from rest_framework import routers
from rest_framework.authtoken import views as token_views
from rest_framework.documentation import include_docs_urls

from main_app import views
from main_app import custom_views

router = routers.DefaultRouter()
router.register(r'webook_user', views.WeBookUserViewSet)
router.register(r'user_rating', views.UserRatingViewSet)
router.register(r'private_message', views.PrivateMessageViewSet)
router.register(r'image_pool', views.ImagePoolViewSet)
router.register(r'shipping_provider', views.ShippingProviderViewSet)
router.register(r'book_category', views.BookCategoryViewSet)
router.register(r'book', views.BookViewSet)
router.register(r'book_review', views.BookReviewViewSet)
router.register(r'post', views.PostViewSet)
router.register(r'post_comment', views.PostCommentViewSet)
router.register(r'post_like', views.PostLikeViewSet)
router.register(r'post_notification', views.PostNotificationViewSet)
router.register(r'report', views.ReportViewSet)
router.register(r'purchase_order_item', views.PurchaseOrderItemViewSet)
router.register(r'purchase_order', views.PurchaseOrderViewSet)
router.register(r'debug_post', views.DebugPostViewset)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('accounts/', include('allauth.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', token_views.obtain_auth_token),
    path('docs/', include_docs_urls(title='My API title')),
    path('custom/paypal', custom_views.paypal, name='paypal'),
    path('custom/paypal_transaction_complete', custom_views.paypal_transaction_complete, name='paypal_transaction_complete'),
    path('custom/thaipost_tracking_bridge', custom_views.thaipost_tracking_bridge, name='thaipost_tracking_bridge')
]
