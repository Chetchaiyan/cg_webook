import json
import datetime

from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory
from django.core.files.uploadedfile import SimpleUploadedFile

from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient

from main_app.models import *
from main_app.views import *
from main_app.custom_views import *

class TestPayPal(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.client = APIClient()
        response = self.client.post('/webook_user/register/',
                    {'username': 'test_user_1', 'email': 'test1@gmail.com', 'password1' : 'abCD8932',
                    'password2' : 'abCD8932', 'fullname':'Chet Chetchaiyan', 'address': '21',
                    'phone_number':'085-115-3030', 'country':'Thailand'},
                    format='json')
        self.user_1 = User.objects.get( pk=response.data['user']['id'] )

    def test_request_paypal(self):
        book_1 = Book(user=self.user_1, publication_year=2017, geo_lat=0, geo_long=0, weight='1')
        book_1.save()
        book_2 = Book(user=self.user_1, publication_year=2017, geo_lat=0, geo_long=0, weight='1')
        book_2.save()
        po = PurchaseOrder(user=self.user_1, payment='paypal', shipping_address='')
        po.save()
        po_item_1 = PurchaseOrderItem(purchase_order=po, item=book_1, price=9.99)
        po_item_1.save()
        po_item_2 = PurchaseOrderItem(purchase_order=po, item=book_2, price=9.99)
        po_item_2.save()
        request = self.factory.get('/custom/paypal?po={}'.format(po.id))
        response = paypal(request)
        self.assertEqual(response.status_code, 200)

    def test_paid(self):
        book_1 = Book(user=self.user_1, publication_year=2017, geo_lat=0, geo_long=0, weight='1')
        book_1.save()
        book_2 = Book(user=self.user_1, publication_year=2017, geo_lat=0, geo_long=0, weight='1')
        book_2.save()
        po = PurchaseOrder(user=self.user_1, payment='paypal', shipping_address='')
        po.save()
