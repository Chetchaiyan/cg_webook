import json
import datetime

from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory
from django.core.files.uploadedfile import SimpleUploadedFile

from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient

from main_app.models import *
from main_app.views import *

def basic_user_setup(testcase):
    testcase.client = APIClient()
    response = testcase.client.post('/webook_user/register/',
                {'username': 'test_user_1', 'email': 'test1@gmail.com', 'password1' : 'abCD8932',
                'password2' : 'abCD8932', 'fullname':'Chet Chetchaiyan', 'address': '21',
                'phone_number':'085-115-3030', 'country':'Thailand'},
                format='json')
    testcase.user_1 = User.objects.get( pk=response.data['user']['id'] )
    testcase.webook_user_1 = WeBookUser.objects.get( pk=testcase.user_1 )
    response = testcase.client.post('/webook_user/register/',
                {'username': 'test_user_2', 'email': 'test2@gmail.com', 'password1' : 'abCD8932',
                'password2' : 'abCD8932', 'country': 'Japan'},
                format='json')
    testcase.user_2 = User.objects.get( pk=response.data['user']['id'] )
    testcase.webook_user_2 = WeBookUser.objects.get( pk=testcase.user_2 )
    testcase.client.force_authenticate(user=testcase.user_1)

class TestWeBookUserRegistrationView(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_register(self):
        response = self.client.post('/webook_user/register/',
                    {'username': 'test', 'email': 'test@gmail.com', 'password1' : 'abCD8932', 'password2' : 'abCD8932'},
                    format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['user']['username'], 'test')

    def test_register_then_login(self):
        response = self.client.post('/webook_user/register/',
                    {'username': 'test', 'password1' : 'abCD8932', 'email': 'test@gmail.com', 'password2' : 'abCD8932'},
                    format='json')
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/api-token-auth/',
                    {'username': 'test@gmail.com', 'password' : 'abCD8932'}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue( 'token' in response.data )
        response = self.client.post('/api-token-auth/',
                    {'username': 'test', 'password' : 'abCD8932'}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue( 'token' in response.data )

    def test_register_with_additional_param(self):
        response = self.client.post('/webook_user/register/',
                    {'username': 'test', 'password1' : 'abCD8932', 'email': 'test@gamil.com',
                    'gender' : 'male', 'password2' : 'abCD8932'},
                    format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['gender'], 'male')
        self.assertEqual(response.data['user']['email'], 'test@gamil.com')

    def test_register_with_duplicate_username(self):
        response = self.client.post('/webook_user/register/',
                    {'username': 'test', 'email': 'test@gmail.com', 'password1': 'abCD8932', 'password2': 'abCD8932'},
                    format='json')
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/webook_user/register/',
                    {'username': 'test', 'email': 'test2@gmail.com', 'password1': 'abCD8932', 'password2': 'abCD8932'},
                    format='json')
        self.assertEqual(response.status_code, 400)

    def test_register_with_duplicate_email(self):
        response = self.client.post('/webook_user/register/',
                    {'username': 'test', 'email': 'test@gmail.com', 'password1': 'abCD8932', 'password2': 'abCD8932'},
                    format='json')
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/webook_user/register/',
                    {'username': 'test2', 'email': 'test@gmail.com', 'password1': 'abCD8932', 'password2': 'abCD8932'},
                    format='json')
        self.assertEqual(response.status_code, 400)

class TestWebBookUserView(TestCase):
    def setUp(self):
        basic_user_setup(self)

    def test_social_login(self):
        self.webook_user_1.social_fb_id = 'fake_facebook_id'
        self.webook_user_1.save()
        response = self.client.post('/webook_user/social_login/', {'social': 'fb', 'id':'fake_facebook_id'}, format='json')
        print(response.data)
        self.assertEqual(response.status_code, 200)

    def test_social_login_fail(self):
        response = self.client.post('/webook_user/social_login/', {'social': 'fb', 'id':'fake_facebook_id'}, format='json')
        print(response.data)
        self.assertEqual(response.status_code, 400)

    def test_create_user_by_post(self):
        response = self.client.post('/webook_user/', {'address': 'update new address.'}, format='json')
        self.assertEqual(response.status_code, 405)

    # test_update_profile (self)
    def test_update_profile_self(self):
        response = self.client.patch('/webook_user/{}/'.format(self.user_1.id),
                    {'address': 'update new address.', 'gender':'male'}, format='json')
        self.assertEqual(response.status_code, 200)
        response_dict = json.loads( response.content )
        self.assertEqual('update new address.', response_dict['address'])
        self.assertEqual('male', response_dict['gender'])

    def test_update_user_profile_email(self):
        response = self.client.patch('/webook_user/{}/'.format(self.user_1.id),
                    {'user': { 'id': self.user_1.id, 'email':'new@mail.com'}, 'email': 'test_out@gmail.com', 'address': 'update new address.'}, format='json')
        self.assertEqual(response.status_code, 200)
        response_dict = json.loads( response.content )
        self.assertEqual('new@mail.com', response_dict['user']['email'])

    def test_update_profile_other(self):
        response = self.client.patch('/webook_user/{}/'.format(self.user_2.id),
                    {'address': 'update new address.'}, format='json')
        self.assertEqual(response.status_code, 403)

    def test_add_friend(self):
        response = self.client.post('/webook_user/add_friend/',
                    {'friend': self.user_2.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual('Add Friend Success.', response.data['detail'] )
        user_2 = User.objects.get(pk=self.user_2.id)
        self.client.force_authenticate(user=user_2)
        response = self.client.get('/webook_user/?friend_relation=friend_request', {}, format='json')
        self.assertEqual(response.status_code, 200)
        user_id_list = [ u['user']['id'] for u in response.data['results']]
        self.assertIn(self.user_1.id, user_id_list)

    def test_remove_friend(self):
        webook_user = WeBookUser.objects.get(pk=self.user_1.id)
        webook_user.friend.add(self.user_2.id)
        webook_user.save()
        response = self.client.get('/webook_user/?friend_relation=friend', {}, format='json')
        response_id_list = [ i['user']['id'] for i in response.data['results'] ]
        self.assertIn(self.user_2.id, response_id_list)
        response = self.client.post('/webook_user/remove_friend/',
                    {'friend': self.user_2.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual('Remove Friend Success.', response.data['detail'] )
        response = self.client.get('/webook_user/?friend_relation=friend', {}, format='json')
        response_id_list = [ i['user']['id'] for i in response.data['results'] ]
        self.assertNotIn(self.user_2.id, response_id_list)

    def test_accept_friend_fail(self):
        response = self.client.post('/webook_user/accept_friend/',
                    {'friend': self.user_2.id}, format='json')
        self.assertEqual(response.status_code, 400)

    def test_accept_friend_success(self):
        response = self.client.post('/webook_user/add_friend/',
                    {'friend': self.user_2.id}, format='json')
        user_2 = User.objects.get(pk=self.user_2.id)
        self.client.force_authenticate(user=user_2)
        response = self.client.post('/webook_user/accept_friend/',
                    {'friend': self.user_1.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertIn(self.user_2.id, self.webook_user_1.friend.all().values_list('user_id', flat=True))
        self.assertIn(self.user_1.id, self.webook_user_2.friend.all().values_list('user_id', flat=True))

    def test_reject_friend__request_success(self):
        response = self.client.post('/webook_user/add_friend/',
                    {'friend': self.user_2.id}, format='json')
        user_2 = User.objects.get(pk=self.user_2.id)
        self.client.force_authenticate(user=user_2)
        response = self.client.post('/webook_user/reject_friend_request/',
                    {'friend': self.user_1.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(self.user_2.id, self.webook_user_1.friend.all().values_list('user_id', flat=True))
        self.assertNotIn(self.user_2.id, self.webook_user_1.friend_requesting.all().values_list('user_id', flat=True))

    def test_reject_friend_request_fail(self):
        response = self.client.post('/webook_user/reject_friend_request/',
                    {'friend': self.user_2.id}, format='json')
        self.assertEqual(response.status_code, 400)

    def test_block_friend_not_friend(self):
        response = self.client.post('/webook_user/block_friend/',
                    {'friend': self.user_2.id}, format='json')
        self.assertEqual(response.status_code, 400)

    def test_block_friend_friend(self):
        self.webook_user_1.friend.add(self.user_2.id)
        response = self.client.post('/webook_user/block_friend/',
                    {'friend': self.user_2.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(self.user_2.id, self.webook_user_1.friend.all().values_list('user_id', flat=True))

    def test_unblock_friend_fail(self):
        response = self.client.post('/webook_user/unblock_friend/',
                    {'friend': self.user_2.id}, format='json')
        self.assertEqual(response.status_code, 400)

    def test_unblock_friend_success(self):
        self.webook_user_1.friend_blocking.add( self.user_2.id )
        response = self.client.post('/webook_user/unblock_friend/',
                    {'friend': self.user_2.id}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(self.user_2.id, self.webook_user_1.friend_blocking.all().values_list('user_id', flat=True))
        self.assertIn(self.user_2.id, self.webook_user_1.friend.all().values_list('user_id', flat=True))

    def test_favorite_book(self):
        response = self.client.get('/webook_user/book_favorite/', {}, format='json')
        self.assertEqual(response.status_code, 200)

    def test_get_friend(self):
        self.webook_user_1.friend.add( self.user_2.id )
        response = self.client.get('/webook_user/?friend_relation=friend', {}, format='json')
        self.assertEqual(response.status_code, 200)
        response_id_list = [ i['user']['id'] for i in response.data['results'] ]
        self.assertIn(self.user_2.id, response_id_list)

    def test_get_friend_blocking(self):
        self.webook_user_1.friend_blocking.add( self.user_2.id )
        response = self.client.get('/webook_user/?friend_relation=friend_blocking', {}, format='json')
        self.assertEqual(response.status_code, 200)
        response_id_list = [ i['user']['id'] for i in response.data['results'] ]
        self.assertIn(self.user_2.id, response_id_list)

    def test_get_friend_requesting(self):
        self.webook_user_1.friend_requesting.add( self.user_2.id )
        response = self.client.get('/webook_user/?friend_relation=friend_requesting', {}, format='json')
        self.assertEqual(response.status_code, 200)
        response_id_list = [ i['user']['id'] for i in response.data['results'] ]
        self.assertIn(self.user_2.id, response_id_list)

    def test_get_self_photo_empty(self):
        response = self.client.get('/webook_user/{}/photo/'.format(self.user_1.id), {})
        self.assertEqual(response.status_code, 200)

    def test_get_self_photo_with_return(self):
        self.webook_user_1.photo = SimpleUploadedFile(name='for_test_upload.jpg',
                    content=open('for_test_upload.jpg', 'rb').read(), content_type='image/jpeg')
        self.webook_user_1.save()
        response = self.client.get('/webook_user/{}/photo/'.format(self.user_1.id), {})
        self.assertEqual(response._headers['content-type'][1], 'image/jpeg')
        self.assertEqual(response.status_code, 200)

    def test_get_other_photo(self):
        self.webook_user_2.photo = SimpleUploadedFile(name='for_test_upload.jpg',
                    content=open('for_test_upload.jpg', 'rb').read(), content_type='image/jpeg')
        self.webook_user_2.save()
        response = self.client.get('/webook_user/{}/photo/'.format(self.user_2.id), {})
        self.assertEqual(response._headers['content-type'][1], 'image/jpeg')
        self.assertEqual(response.status_code, 200)

    def test_change_password_success(self):
        response = self.client.post('/webook_user/change_password/',
                    {'old_password': 'abCD8932', 'new_password':'hello'}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue( self.user_1.check_password('hello') )

    def test_change_password_fail_wrong_old_password(self):
        response = self.client.post('/webook_user/change_password/',
                    {'old_password': 'wrong_password', 'new_password':'hello'}, format='json')
        self.assertEqual(response.status_code, 400)
        self.assertFalse( self.user_1.check_password('hello') )

    def test_forget_password_fail_wrong_email(self):
        self.client.force_authenticate(user=None)
        response = self.client.post('/webook_user/forget_password/',
                    {'username': 'test_user_1', 'email':'wrong@gmail.com'}, format='json')
        self.assertEqual(response.status_code, 400)

    def test_forget_password_fail_wrong_username(self):
        self.client.force_authenticate(user=None)
        response = self.client.post('/webook_user/forget_password/',
                    {'username': 'fake_username', 'email':'wrong@gmail.com'}, format='json')
        self.assertEqual(response.status_code, 400)

    def test_forget_password_success(self):
        self.client.force_authenticate(user=None)
        response = self.client.post('/webook_user/forget_password/',
                    {'username': 'test_user_1', 'email':'test1@gmail.com'}, format='json')
        self.assertEqual(response.status_code, 200)

    ''' ToDo : Write proper test
    def test_push(self):
        from chet_util import push_gears
        push_gears.push_all("Hello test push all", {'dummy_data': 1})
        push_gears.push_target(35, "Hello test push target", {'dummy_data': 1})
    '''

    def test_coin_to_announcement_fail(self):
        response = self.client.post('/webook_user/coin_to_announcement/', {}, format='json')
        self.assertEqual(response.status_code, 400)

    def test_coin_to_announcement_success(self):
        self.webook_user_1.coin = 100
        self.webook_user_1.save()
        announcement_credit = self.webook_user_1.announcement_credit
        response = self.client.post('/webook_user/coin_to_announcement/', {}, format='json')
        self.assertEqual(response.status_code, 200)
        self.webook_user_1 = WeBookUser.objects.get(pk=self.webook_user_1.user)
        self.assertEqual(100 - 65, self.webook_user_1.coin)
        self.assertEqual(announcement_credit + 1, self.webook_user_1.announcement_credit)

class TestWeBookUserLevel(TestCase):
    def setUp(self):
        basic_user_setup(self)
        response = self.client.post('/webook_user/register/',
                    {'username': 'test_user_3', 'email': 'test3@gmail.com', 'password1' : 'abCD8932', 'password2' : 'abCD8932'},
                    format='json')
        self.user_3 = User.objects.get( pk=response.data['user']['id'] )
        self.webook_user_3 = WeBookUser.objects.get( pk=self.user_3 )
        response = self.client.post('/webook_user/register/',
                    {'username': 'test_user_4', 'email': 'test4@gmail.com', 'password1' : 'abCD8932', 'password2' : 'abCD8932'},
                    format='json')
        self.user_4 = User.objects.get( pk=response.data['user']['id'] )
        self.webook_user_4 = WeBookUser.objects.get( pk=self.user_4 )
        response = self.client.post('/webook_user/register/',
                    {'username': 'test_user_5', 'email': 'test5@gmail.com', 'password1' : 'abCD8932', 'password2' : 'abCD8932'},
                    format='json')
        self.user_5 = User.objects.get( pk=response.data['user']['id'] )
        self.webook_user_5 = WeBookUser.objects.get( pk=self.user_5 )
        response = self.client.post('/webook_user/register/',
                    {'username': 'test_user_6', 'email': 'test6@gmail.com', 'password1' : 'abCD8932', 'password2' : 'abCD8932'},
                    format='json')
        self.user_6 = User.objects.get( pk=response.data['user']['id'] )
        self.webook_user_6 = WeBookUser.objects.get( pk=self.user_6 )

    def test_default_level(self):
        self.assertEqual(self.webook_user_1.level, 0)

    def test_five_rating(self):
        self.client.force_authenticate(user=self.user_2)
        response = self.client.post('/user_rating/', {'target': self.user_1.id, 'rating': 5}, format='json')
        self.assertEqual(response.status_code, 201)
        self.client.force_authenticate(user=self.user_3)
        response = self.client.post('/user_rating/', {'target': self.user_1.id, 'rating': 5}, format='json')
        self.assertEqual(response.status_code, 201)
        self.client.force_authenticate(user=self.user_4)
        response = self.client.post('/user_rating/', {'target': self.user_1.id, 'rating': 5}, format='json')
        self.assertEqual(response.status_code, 201)
        self.client.force_authenticate(user=self.user_5)
        response = self.client.post('/user_rating/', {'target': self.user_1.id, 'rating': 5}, format='json')
        self.assertEqual(response.status_code, 201)
        self.client.force_authenticate(user=self.user_5)
        response = self.client.post('/user_rating/', {'target': self.user_1.id, 'rating': 5}, format='json')
        self.assertEqual(response.status_code, 201)
        self.webook_user_1.save()
        self.assertEqual(self.webook_user_1.level, 1)
        self.assertEqual(self.webook_user_1.recommend, 75)

class TestPrivateMessageView(TestCase):
    def setUp(self):
        basic_user_setup(self)

    def test_private_message(self):
        response = self.client.post('/private_message/send/',
                    {'receiver': self.user_2.id, 'message' : 'Hello PM 1.'}, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/private_message/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_send_private_message(self):
        response = self.client.post('/private_message/send/',
                    {'receiver': self.user_2.id, 'message' : 'Hello PM 1.'}, format='json')
        self.assertEqual(response.status_code, 200)

    def test_read_private_message(self):
        response = self.client.post('/private_message/send/',
                    {'receiver': self.user_2.id, 'message' : 'Hello PM 1.'}, format='json')
        response = self.client.post('/private_message/send/',
                    {'receiver': self.user_2.id, 'message' : 'Hello PM 2.'}, format='json')
        response = self.client.get('/private_message/{}/read/'.format(self.user_2.id), {}, format='json')
        self.assertEqual(response.status_code, 200)
        message_list = [ pm['message'] for pm in response.data ]
        self.assertListEqual(['Hello PM 2.', 'Hello PM 1.'], message_list)
        user_2 = User.objects.get( pk=self.user_2.id )
        self.client.force_authenticate(user=user_2)
        response = self.client.get('/private_message/{}/read/'.format(self.user_1.id), {}, format='json')
        self.assertEqual(response.status_code, 200)
        message_list = [ pm['message'] for pm in response.data ]
        self.assertListEqual(['Hello PM 2.', 'Hello PM 1.'], message_list)

    def test_clear_message(self):
        response = self.client.post('/private_message/send/',
                    {'receiver': self.user_2.id, 'message' : 'Hello PM 1.'}, format='json')
        response = self.client.post('/private_message/send/',
                    {'receiver': self.user_2.id, 'message' : 'Hello PM 2.'}, format='json')
        response = self.client.post('/private_message/clear/',
                    {'conversation_user': self.user_2.id}, format='json')
        response = self.client.get('/private_message/{}/read/'.format(self.user_2.id), {}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertListEqual([], response.data)

class TestBookView(TestCase):
    def setUp(self):
        basic_user_setup(self)
        self.book_cat = BookCategory3(name="test_category")
        self.book_cat.save()
        self.shipping_provider = ShippingProvider(name='DHL')
        self.shipping_provider.save()

    def test_create_book(self):
        response = self.client.post('/book/',
                    {'title': 'title', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment', 'shipping_provider': [self.shipping_provider.name]}, format='json')
        self.assertEqual(response.status_code, 201)

        book_id = response.data['id']
        book = Book.objects.get(pk=book_id)
        self.assertEqual('test_user_1', book.user.username)
        self.assertEqual('condition', book.condition)
        self.assertEqual(self.user_1.id, response.data['user_id'])

    def test_add_favorite(self):
        response = self.client.post('/book/',
                    {'title': 'title', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment', 'shipping_provider': [self.shipping_provider.name]}, format='json')
        book_id = response.data['id']
        response = self.client.post('/book/{}/add_favorite/'.format(book_id), format='json')
        self.assertEqual(response.status_code, 200)
        favorite = [ f.id for f in self.webook_user_1.favorite_book.all() ]
        self.assertListEqual([book_id], favorite)

    def test_remove_favorite(self):
        response = self.client.post('/book/',
                    {'title': 'title', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment', 'shipping_provider': [self.shipping_provider.name]}, format='json')
        book_id = response.data['id']
        response = self.client.post('/book/{}/add_favorite/'.format(book_id), format='json')
        response = self.client.post('/book/{}/remove_favorite/'.format(book_id), format='json')
        self.assertEqual(response.status_code, 200)
        favorite = [ f.id for f in self.webook_user_1.favorite_book.all() ]
        self.assertListEqual([], favorite)

    def test_book_price_structure(self):
        response = self.client.get('/book/shipping_price_datadict/')
        self.assertEqual(response.status_code, 200)

class TestBookOrderAndFilter(TestCase):
    def setUp(self):
        basic_user_setup(self)
        self.book_cat = BookCategory3(name="test_category")
        self.book_cat.save()
        self.shipping_provider = ShippingProvider(name="DHL")
        self.shipping_provider.save()
        response = self.client.post('/book/',
                    {'title': 'titleA', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')
        self.book_a = response.data['id']
        response = self.client.post('/book/',
                    {'title': 'titleC', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')
        self.book_c = response.data['id']
        response = self.client.post('/book/',
                    {'title': 'titleB', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')
        self.book_b = response.data['id']
        self.client.force_authenticate(user=self.user_2)
        response = self.client.post('/book/',
                    {'title': 'titleE', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')
        response = self.client.post('/book/',
                    {'title': 'titleG', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')
        response = self.client.post('/book/',
                    {'title': 'titleH', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')
        self.client.force_authenticate(user=self.user_1)
        response = self.client.post('/book/',
                    {'title': 'titleI', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')
        response = self.client.post('/book/',
                    {'title': 'titleF', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')

    def test_order_by_title(self):
        title_order = ['titleA', 'titleB', 'titleC', 'titleE', 'titleF', 'titleG', 'titleH', 'titleI', ]
        response = self.client.get('/book/?ordering=title', format='json')
        self.assertEqual(response.status_code, 200)
        book_id_list = [ b['title'] for b in response.data['results']]
        self.assertListEqual(title_order, book_id_list)
        response = self.client.get('/book/?ordering=-title', format='json')
        self.assertEqual(response.status_code, 200)
        book_id_list = [ b['title'] for b in response.data['results']]
        self.assertListEqual(title_order[::-1], book_id_list)

    def test_order_by_create_dt(self):
        create_dt_order = ['titleA', 'titleC', 'titleB', 'titleE', 'titleG', 'titleH', 'titleI', 'titleF']
        response = self.client.get('/book/?ordering=create_dt', format='json')
        self.assertEqual(response.status_code, 200)
        book_id_list = [ b['title'] for b in response.data['results']]
        self.assertListEqual(create_dt_order, book_id_list)
        response = self.client.get('/book/?ordering=-create_dt', format='json')
        self.assertEqual(response.status_code, 200)
        book_id_list = [ b['title'] for b in response.data['results']]
        self.assertListEqual(create_dt_order[::-1], book_id_list)

    def test_order_by_most_review(self):
        response = self.client.post('/book_review/',
                    {'book': self.book_a, 'review_text': 'hello', 'rating': 5}, format='json')
        response = self.client.post('/book_review/',
                    {'book': self.book_b, 'review_text': 'hello', 'rating': 4}, format='json')
        self.client.force_authenticate(user=self.user_2)
        response = self.client.post('/book_review/',
                    {'book': self.book_b, 'review_text': 'hello', 'rating': 4}, format='json')
        response = self.client.get('/book/?ordering=-review_count', {}, format='json')
        self.assertEqual(response.status_code, 200)
        book_id_list = [ b['title'] for b in response.data['results']]
        self.assertListEqual(['titleB', 'titleA'], book_id_list[:2])

    def test_filter_by_closest(self):
        closest_filter = ['titleA', 'titleC', 'titleB', 'titleI', 'titleF']
        response = self.client.get('/book/?closest=t', format='json')
        self.assertEqual(response.status_code, 200)
        book_id_list = [ b['title'] for b in response.data['results']]
        self.assertListEqual(closest_filter[::-1], book_id_list)

class TestBookReviewView(TestCase):
    def setUp(self):
        basic_user_setup(self)
        self.book_cat = BookCategory3(name="test_category")
        self.book_cat.save()
        self.shipping_provider = ShippingProvider(name="DHL")
        self.shipping_provider.save()
        response = self.client.post('/book/',
                    {'title': 'titleA', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment', 'shipping_provider': [self.shipping_provider.name]}, format='json')
        self.book_id = response.data['id']

    def test_create_book_review(self):
        response = self.client.post('/book_review/',
                    {'book': self.book_id, 'review_text': 'hello', 'rating': 4}, format='json')
        self.assertEqual(response.status_code, 201)
        book_reivew = BookReview.objects.get(user=self.user_1.id, book=self.book_id)
        self.assertEqual('hello', book_reivew.review_text)

    def test_create_duplicate_book_review(self):
        response = self.client.post('/book_review/',
                    {'book': self.book_id, 'review_text': 'hello', 'rating': 4}, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.client.post('/book_review/',
                    {'book': self.book_id, 'review_text': 'hello', 'rating': 4}, format='json')
        self.assertEqual(response.status_code, 400)

    def test_book_rating(self):
        response = self.client.post('/book_review/',
                    {'book': self.book_id, 'review_text': 'hello', 'rating': 5}, format='json')
        self.client.force_authenticate(user=self.user_2)
        response = self.client.post('/book_review/',
                    {'book': self.book_id, 'review_text': 'hello', 'rating': 4}, format='json')
        response = self.client.get('/book/{}/'.format(self.book_id), {}, format='json')
        self.assertEqual(4.5, response.data['rating'])

    def test_book_rating_no_rating(self):
        response = self.client.get('/book/{}/'.format(self.book_id), {}, format='json')
        self.assertEqual(0, response.data['rating'])

class TestPostView(TestCase):
    def setUp(self):
        basic_user_setup(self)

    def test_create_post(self):
        response = self.client.post('/post/', {'post_text': 'text', 'post_exposure' : 'public'}, format='json')
        self.assertEqual(response.status_code, 201)
        post = Post.objects.get(pk=response.data['id'])
        self.assertEqual('test_user_1', post.user.username)
        self.assertEqual('public', post.post_exposure)

    def test_create_post_announcement(self):
        self.webook_user_1.announcement_credit = 10
        self.webook_user_1.save()
        response = self.client.post('/post/', {'post_text': 'text_a', 'post_exposure' : 'announcement'}, format='json')
        self.assertEqual(response.status_code, 201)
        post = Post.objects.get(pk=response.data['id'])
        self.assertEqual('test_user_1', post.user.username)
        self.assertEqual('announcement', post.post_exposure)
        response = self.client.get('/post/')
        post_text_list = [ p['post_text'] for p in response.data['results'] ]
        self.assertListEqual(['text_a'], post_text_list)
        response = self.client.get('/post/')
        post_text_list = [ p['post_text'] for p in response.data['results'] ]
        self.assertListEqual(['text_a'], post_text_list)

    def test_default_ordering(self):
        response = self.client.post('/post/', {'post_text': 'text_1', 'post_exposure' : 'public'}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_2', 'post_exposure' : 'public'}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_3', 'post_exposure' : 'public'}, format='json')
        response = self.client.get('/post/')
        post_text_list = [ p['post_text'] for p in response.data['results'] ]
        self.assertListEqual(['text_3', 'text_2', 'text_1'], post_text_list)

    def test_post_image_delete(self):
        response = self.client.post('/post/', {'post_text': 'text', 'post_exposure' : 'public'}, format='json')
        post = Post.objects.get(pk=response.data['id'])
        post.image = SimpleUploadedFile(name='for_test_upload.jpg',
                    content=open('for_test_upload.jpg', 'rb').read(), content_type='image/jpeg')
        post.save()
        response = self.client.delete('/post/{}/image/'.format(post.id))
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/post/{}/image/'.format(post.id))
        self.assertNotEqual(response._headers['content-type'][1], 'image/jpeg')
        self.assertEqual(response.status_code, 200)

    def test_friend_false(self):
        response = self.client.post('/post/', {'post_text': 'text', 'post_exposure' : 'public'}, format='json')
        post = Post.objects.get(pk=response.data['id'])
        self.client.force_authenticate(user=self.user_2)
        response = self.client.get('/post/{}/'.format(post.id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['is_user_friend'], False)

    def test_friend_true(self):
        response = self.client.post('/post/', {'post_text': 'text', 'post_exposure' : 'public'}, format='json')
        post = Post.objects.get(pk=response.data['id'])
        self.webook_user_2.friend.add(self.webook_user_1)
        self.client.force_authenticate(user=self.user_2)
        response = self.client.get('/post/{}/'.format(post.id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['is_user_friend'], True)

class TestPostFilter(TestCase):
    def setUp(self):
        basic_user_setup(self)
        response = self.client.post('/webook_user/register/',
                    {'username': 'test_user_3', 'email': 'test3@gmail.com', 'password1' : 'abCD8932', 'password2' : 'abCD8932'},
                    format='json')
        self.user_3 = User.objects.get( pk=response.data['user']['id'] )
        self.webook_user_3 = WeBookUser.objects.get( pk=self.user_3 )
        response = self.client.post('/post/', {'post_text': 'text_1', 'post_exposure' : 'public'}, format='json')
        post = Post.objects.get(pk=response.data['id'])
        post.image = SimpleUploadedFile(name='for_test_upload.jpg',
                    content=open('for_test_upload.jpg', 'rb').read(), content_type='image/jpeg')
        post.save()
        self.webook_user_1.announcement_credit = 10
        self.webook_user_1.save()
        self.webook_user_2.announcement_credit = 10
        self.webook_user_2.save()
        self.webook_user_3.announcement_credit = 10
        self.webook_user_3.save()
        response = self.client.post('/post/', {'post_text': 'text_2', 'post_exposure' : 'announcement',
                    'geo_lat':100.1, 'geo_long':13.6}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_3', 'post_exposure' : 'public'}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_4', 'post_exposure' : 'announcement'}, format='json')
        post = Post.objects.get(pk=response.data['id'])
        post.image = SimpleUploadedFile(name='for_test_upload.jpg',
                    content=open('for_test_upload.jpg', 'rb').read(), content_type='image/jpeg')
        post.save()
        self.client.force_authenticate(user=self.user_2)
        response = self.client.post('/post/', {'post_text': 'text_5', 'post_exposure' : 'public'}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_6', 'post_exposure' : 'announcement',
                    'geo_lat':180.1, 'geo_long':13.6}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_7', 'post_exposure' : 'public'}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_8', 'post_exposure' : 'announcement',
                    'geo_lat':100.1, 'geo_long':13.6}, format='json')
        self.client.force_authenticate(user=self.user_3)
        response = self.client.post('/post/', {'post_text': 'text_9', 'post_exposure' : 'public'}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_10', 'post_exposure' : 'announcement',
                    'geo_lat':100.1, 'geo_long':13.6}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_11', 'post_exposure' : 'public'}, format='json')
        response = self.client.post('/post/', {'post_text': 'text_12', 'post_exposure' : 'announcement',
                    'geo_lat':100.1, 'geo_long':13.6}, format='json')
        post = Post.objects.get(pk=response.data['id'])
        post.image = SimpleUploadedFile(name='for_test_upload.jpg',
                    content=open('for_test_upload.jpg', 'rb').read(), content_type='image/jpeg')
        post.save()
        self.client.force_authenticate(user=self.user_1)
        self.webook_user_1.friend.add(self.webook_user_2)
        self.webook_user_2.friend.add(self.webook_user_1)

    def test_filter_post_by_user(self):
        result = ['text_4', 'text_3', 'text_2', 'text_1']
        response = self.client.get('/post/?post_by={}'.format(self.user_1.id))
        self.assertEqual(response.status_code, 200)
        post_text_list = [ p['post_text'] for p in response.data['results']]
        self.assertListEqual(result, post_text_list)

    def test_filter_has_image(self):
        result = ['text_12', 'text_4', 'text_1']
        response = self.client.get('/post/?has_image=1')
        self.assertEqual(response.status_code, 200)
        post_text_list = [ p['post_text'] for p in response.data['results']]
        self.assertListEqual(result, post_text_list)

    def test_filter_post_by_friend(self):
        result = ['text_8', 'text_7', 'text_6', 'text_5']
        response = self.client.get('/post/?friend={}'.format(self.user_1.id))
        self.assertEqual(response.status_code, 200)
        post_text_list = [ p['post_text'] for p in response.data['results']]
        self.assertListEqual(result, post_text_list)

    def test_filter_exposure(self):
        result = ['text_11', 'text_9', 'text_7', 'text_5', 'text_3', 'text_1']
        response = self.client.get('/post/?post_exposure=public')
        self.assertEqual(response.status_code, 200)
        post_text_list = [ p['post_text'] for p in response.data['results']]
        self.assertListEqual(result, post_text_list)

    def test_post_is_active_new_create(self):
        response = self.client.post('/post/', {'post_text': 'text_13', 'post_exposure' : 'announcement'}, format='json')
        self.assertEqual(response.data['is_active'], True)

    def test_post_is_active_new_create(self):
        result = ['text_8', 'text_7', 'text_6', 'text_1']
        response = self.client.post('/post/', {'post_text': 'text_13', 'post_exposure' : 'announcement'}, format='json')
        response = self.client.get('/post/', format='json')
        new_post = [ i for i in response.data['results'] if i['post_text'] == 'text_13' ][0]
        print(new_post)
        # ToDo : assert

    def test_post_announcement_by_location(self):
        result = ['text_8', 'text_7', 'text_5']
        self.webook_user_1.current_lat = 100.0
        self.webook_user_1.current_long =  13.5
        self.webook_user_1.save()
        response = self.client.get('/post/?post_by={}'.format(self.user_2.id), format='json')
        post_text_list = [ p['post_text'] for p in response.data['results']]
        self.assertListEqual(result, post_text_list)
        # call post list
        # check list against user location

class TestPostComment(TestCase):
    def setUp(self):
        basic_user_setup(self)
        response = self.client.post('/post/', {'post_text': 'text_1', 'post_exposure' : 'public'}, format='json')
        self.post_1_user_1 = Post.objects.get(pk=response.data['id'])
        response = self.client.post('/post/', {'post_text': 'text_2', 'post_exposure' : 'public'}, format='json')
        self.client.force_authenticate(user=self.user_2)
        self.post_2_user_1 = Post.objects.get(pk=response.data['id'])
        response = self.client.post('/post/', {'post_text': 'text_3', 'post_exposure' : 'public'}, format='json')
        self.post_1_user_2 = Post.objects.get(pk=response.data['id'])
        self.client.force_authenticate(user=self.user_1)

    def test_comment(self):
        response = self.client.post('/post_comment/',
                    {'post': self.post_1_user_1.id, 'comment_text' : 'comment_1'}, format='json')
        comment = PostComment.objects.get(pk=response.data['id'])
        self.assertEqual('test_user_1', comment.user.username)
        self.assertEqual('comment_1', comment.comment_text)

    def test_filter_comment_by_post(self):
        response = self.client.post('/post_comment/',
                    {'post': self.post_1_user_1.id, 'comment_text' : 'comment_1'}, format='json')
        response = self.client.post('/post_comment/',
                    {'post': self.post_1_user_1.id, 'comment_text' : 'comment_1'}, format='json')
        response = self.client.post('/post_comment/',
                    {'post': self.post_2_user_1.id, 'comment_text' : 'comment_1'}, format='json')
        response = self.client.get('/post_comment/?post={}&ordering=-create_dt'.format(self.post_1_user_1.id))
        self.assertEqual(2, response.data['count'])
        response = self.client.get('/post/{}/'.format(self.post_1_user_1.id), format='json')
        self.assertEqual(2, response.data['comment_count'])

    def test_comment_count(self):
        response = self.client.post('/post_comment/',
                    {'post': self.post_1_user_1.id, 'comment_text' : 'comment_1'}, format='json')
        response = self.client.post('/post_comment/',
                    {'post': self.post_1_user_1.id, 'comment_text' : 'comment_1'}, format='json')
        response = self.client.get('/post/{}/'.format(self.post_1_user_1.id), format='json')
        self.assertEqual(2, response.data['comment_count'])

class TestPostLike(TestCase):
    def setUp(self):
        basic_user_setup(self)
        response = self.client.post('/post/', {'post_text': 'text_1', 'post_exposure' : 'public'}, format='json')
        self.post = Post.objects.get(pk=response.data['id'])

    def test_post_like(self):
        response = self.client.post('/post_like/', {'user': self.user_2.id, 'post' : self.post.id}, format='json')
        self.assertEqual(response.status_code, 201)

    def test_post_like_property(self):
        response = self.client.post('/post_like/', {'user': self.user_2.id, 'post' : self.post.id}, format='json')
        response = self.client.get('/post/{}/'.format(self.post.id))
        self.assertEqual(1, response.data['like'])

    def test_like_then_unlike(self):
        response = self.client.post('/post_like/', {'user': self.user_2.id, 'post' : self.post.id}, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.client.delete('/post_like/{}/'.format(response.data['id']), format='json')
        self.assertEqual(response.status_code, 204)

    def test_post_is_user_like(self):
        response = self.client.get('/post/{}/'.format(self.post.id), {}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['is_user_like'], 0)
        response = self.client.post('/post_like/', {'user': self.user_1.id, 'post' : self.post.id}, format='json')
        post_like = response.data
        response = self.client.get('/post/{}/'.format(self.post.id), {}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['is_user_like'], post_like['id'])

class TestPostNoti(TestCase):
    def setUp(self):
        basic_user_setup(self)
        response = self.client.post('/post/', {'post_text': 'text_1', 'post_exposure' : 'public'}, format='json')
        self.post = Post.objects.get(pk=response.data['id'])

    def test_post_like(self):
        response = self.client.post('/post_notification/', {'user': self.user_2.id, 'post' : self.post.id},
                    format='json')
        self.assertEqual(response.status_code, 201)

    def test_post_is_user_noti(self):
        response = self.client.get('/post/{}/'.format(self.post.id), {}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['is_user_noti'], 0)
        response = self.client.post('/post_notification/', {'user': self.user_1.id, 'post' : self.post.id},
                    format='json')
        post_noti = response.data
        response = self.client.get('/post/{}/'.format(self.post.id), {}, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['is_user_noti'], post_noti['id'])

class TestReport(TestCase):
    def setUp(self):
        basic_user_setup(self)

    def test_report_post(self):
        response = self.client.post('/post/', {'post_text': 'text_1', 'post_exposure' : 'public'}, format='json')
        post_id = response.data['id']
        response = self.client.post('/report/', {'report_text':'report_1', 'post': post_id}, format='json')
        self.assertEqual(response.status_code, 201)

class TestPurchaseOrder(TestCase):
    def setUp(self):
        basic_user_setup(self)
        self.book_cat = BookCategory3(name="test_category")
        self.book_cat.save()
        self.shipping_provider = ShippingProvider(name='DHL')
        self.shipping_provider.save()
        self.client.force_authenticate(user=self.user_2)
        response = self.client.post('/book/',
                    {'title': 'titleA', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')
        self.book_a = response.data['id']
        response = self.client.post('/book/',
                    {'title': 'titleB', 'condition' : 'condition', 'category':self.book_cat.name, 'author': 'author',
                    'publication_year': 2019, 'book_format':'book_format', 'language': 'TH', 'geo_lat' :0.0,
                    'geo_long':0.0, 'weight':'s', 'owner_comment':'owner_comment',
                    'shipping_provider': [self.shipping_provider.name]}, format='json')
        self.book_b = response.data['id']
        self.client.force_authenticate(user=self.user_1)
        shipping = ShippingProvider(name='ThailandPost')
        shipping.save()
        barcode = ThaipostBarcode(barcode='EB21552502xTH')
        barcode.save()

    def test_create_purchase_order(self):
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid',
                    'po_item': '[{"item": ' + str(self.book_a) + ', "price": 20.99, "shipping_provider": "DHL"}, {"item": ' + str(self.book_b) + ', "price": 9.99, "shipping_provider": "DHL"} ]' })
        self.assertEqual(response.status_code, 201)
        purchase_order = PurchaseOrder.objects.get(pk=response.data['id'])
        self.assertEqual(purchase_order.status, 'not_paid')
        po_item_id_list = [p['id'] for p in response.data['po_item']]
        self.assertEqual(purchase_order.po_item.all().count(), len(po_item_id_list))

    def test_create_purchase_order_double_order(self):
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid',
                    'po_item': '[{"item": ' + str(self.book_a) + ', "price": 20.99, "shipping_provider": "DHL"} ]' })
        self.assertEqual(response.status_code, 201)
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid',
                    'po_item': '[{"item": ' + str(self.book_a) + ', "price": 20.99, "shipping_provider": "DHL"} ]' })
        self.assertEqual(response.status_code, 400)

    def test_create_purchase_order_lock_book(self):
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid',
                    'po_item': '[{"item": ' + str(self.book_a) + ', "price": 20.99, "shipping_provider": "DHL"} ]' })
        self.assertEqual(response.status_code, 201)
        self.client.force_authenticate(user=self.user_2)
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid',
                    'po_item': '[{"item": ' + str(self.book_a) + ', "price": 20.99, "shipping_provider": "DHL"} ]' })
        self.assertEqual(response.status_code, 400)

    def test_order_pay(self):
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid', 'po_item': '[{"item": ' + str(self.book_a) +
                    ',"price": 20.99, "shipping_provider": "ThailandPost"} ]' })
        self.assertEqual(response.status_code, 201)
        order_id = response.data['id']
        po = PurchaseOrder.objects.get(pk=order_id)
        po.paid()
        self.assertEqual(po.status, 'shipping')
        self.assertEqual(po.po_item.all()[0].shipment_status, 'shipping')
        self.assertEqual(po.po_item.all()[0].item.store_status, 'remove')

    def test_order_received_confirm_success(self):
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid', 'po_item': '[{"item": ' + str(self.book_a) +
                    ',"price": 20.99, "shipping_provider": "ThailandPost"} ]' })
        self.assertEqual(response.status_code, 201)
        order_id = response.data['id']
        po = PurchaseOrder.objects.get(pk=order_id)
        po.paid()
        self.assertEqual(po.status, 'shipping')
        self.assertEqual(po.po_item.all()[0].shipment_status, 'shipping')
        response = self.client.post('/purchase_order_item/{}/confirm_package_received/'.format(
                    po.po_item.all()[0].id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(po.po_item.all()[0].shipment_status, 'completed')

    def test_order_received_confirm_fail(self):
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid', 'po_item': '[{"item": ' + str(self.book_a) +
                    ',"price": 20.99, "shipping_provider": "ThailandPost"} ]' })
        self.assertEqual(response.status_code, 201)
        order_id = response.data['id']
        po = PurchaseOrder.objects.get(pk=order_id)
        response = self.client.post('/purchase_order_item/{}/confirm_package_received/'.format(
                    po.po_item.all()[0].id))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(po.po_item.all()[0].shipment_status, 'packing')

    def test_sold_list(self):
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid', 'po_item': '[{"item": ' + str(self.book_a) +
                    ',"price": 20.99, "shipping_provider": "ThailandPost"} ]' })
        self.client.force_authenticate(user=self.user_2)
        response = self.client.get('/purchase_order_item/sold_list/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/purchase_order_item/sold_list/?shipment_status=packing')
        self.assertEqual(response.status_code, 200)

    def test_pay_by_coin(self):
        self.webook_user_1.shipping_credit += 2
        self.webook_user_1.save()
        response = self.client.post('/purchase_order/create_po/', {'payment': 'paypal', 'shipping_address':
                    '21 thailand', 'status': 'not_paid', 'po_item': '[{"item": ' + str(self.book_a) +
                    ',"price": 1.99, "shipping_provider": "ThailandPost"} ]' })
        self.assertEqual(response.status_code, 201)
        order_id = response.data['id']
        response = self.client.post('/purchase_order/{}/coin_paid/'.format(order_id))
        self.assertEqual(response.status_code, 200)
        updated_webook_user = WeBookUser.objects.get(pk=self.webook_user_1.user)
        self.assertEqual(updated_webook_user.shipping_credit, 0)

class TestUserBonus(TestCase):
    def setUp(self):
        basic_user_setup(self)

    def test_get_user_bonus_from_webook_user(self):
        user_bonus = UserBonus()
        user_bonus.user = self.user_1
        user_bonus.bonus_type = 'announcement'
        user_bonus.bonus_value = 5
        user_bonus.expire_date = datetime.datetime.today()
        user_bonus.usable_constrain = False
        user_bonus.usable_remain = 0
        user_bonus.save()
        response = self.client.get('/webook_user/me/')
        self.assertEqual(response.status_code, 200)
        user_bonus = response.data['user_bonus']
        self.assertEqual(user_bonus[0]['bonus_type'], 'announcement')

    def test_bonus_when_level_up(self):
        self.webook_user_1.level_up(1)
        user_bonus = self.webook_user_1.user_bonus.all()
        self.assertEqual(user_bonus.count(), 1)
        bonus = user_bonus.get()
        self.assertEqual(bonus.bonus_value, 5)
        self.assertEqual(bonus.bonus_type, 'announcement')
