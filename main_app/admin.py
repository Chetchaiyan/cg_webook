from django.contrib import admin
from django import forms

from .models import *
# Register your models here.

class ThaipostBarcodeForm(forms.ModelForm):
    barcode = forms.CharField( widget=forms.Textarea, max_length=4096 )

    class Meta:
        model = ThaipostBarcode
        fields = ('barcode', )

@admin.register(ThaipostBarcode)
class ThaipostBarcodeAdmin(admin.ModelAdmin):
    form = ThaipostBarcodeForm
    list_display = ('barcode', 'status')

@admin.register(WeBookUser)
class WeBookUserAdmin(admin.ModelAdmin):
    list_display = ('fullname', 'country_code', 'level', 'coin')
    search_fields = ['fullname', 'user__username', 'user__email', 'paypal_email']

@admin.register(BookCategory3)
class BookCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )

@admin.register(PaymentTransaction)
class PurchaseTransaction(admin.ModelAdmin):
    list_display = ('create_dt', 'payment_category', 'payment_gateway', 'payment_amount')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ('create_dt', 'report_text', 'object_type')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
