from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig


class MainAppConfig(AppConfig):
    name = 'main_app'
    verbose_name = "Main WeBook"

class MainAdminConfig(AdminConfig):
    default_site = 'main_app.admin.MainAdminSite'
