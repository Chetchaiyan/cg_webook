# Generated by Django 2.1.7 on 2019-05-30 04:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0055_auto_20190527_1039'),
    ]

    operations = [
        migrations.AddField(
            model_name='webookuser',
            name='social_fb_id',
            field=models.CharField(max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='webookuser',
            name='social_fb_token',
            field=models.CharField(max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='webookuser',
            name='social_google_id',
            field=models.CharField(max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='webookuser',
            name='social_google_token',
            field=models.CharField(max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='webookuser',
            name='social_line_id',
            field=models.CharField(max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='webookuser',
            name='social_line_token',
            field=models.CharField(max_length=128, null=True),
        ),
    ]
