# Generated by Django 2.1.7 on 2019-05-06 08:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0032_auto_20190506_0846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookreview',
            name='rating',
            field=models.IntegerField(),
        ),
    ]
