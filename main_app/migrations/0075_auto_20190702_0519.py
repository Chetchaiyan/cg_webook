# Generated by Django 2.1.7 on 2019-07-02 05:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0074_auto_20190702_0214'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webookuser',
            name='education',
            field=models.CharField(blank=True, default='', max_length=128, null=True),
        ),
        migrations.AlterField(
            model_name='webookuser',
            name='headline_status',
            field=models.CharField(blank=True, default='', max_length=512, null=True),
        ),
        migrations.AlterField(
            model_name='webookuser',
            name='mood_status',
            field=models.CharField(blank=True, default='', max_length=512, null=True),
        ),
        migrations.AlterField(
            model_name='webookuser',
            name='paypal_email',
            field=models.CharField(blank=True, default='', max_length=128, null=True),
        ),
    ]
