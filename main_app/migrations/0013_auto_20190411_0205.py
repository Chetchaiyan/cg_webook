# Generated by Django 2.1.7 on 2019-04-11 02:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0012_webookuser_favorite_book'),
    ]

    operations = [
        migrations.AddField(
            model_name='webookuser',
            name='friend_blocking',
            field=models.ManyToManyField(related_name='friend_b', to='main_app.WeBookUser'),
        ),
        migrations.AddField(
            model_name='webookuser',
            name='friend_requesting',
            field=models.ManyToManyField(related_name='friend_r', to='main_app.WeBookUser'),
        ),
        migrations.AlterField(
            model_name='webookuser',
            name='friend',
            field=models.ManyToManyField(related_name='friend_user', to='main_app.WeBookUser'),
        ),
    ]
