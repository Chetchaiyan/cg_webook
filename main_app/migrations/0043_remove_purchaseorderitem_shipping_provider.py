# Generated by Django 2.1.7 on 2019-05-16 02:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0042_auto_20190516_0203'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='purchaseorderitem',
            name='shipping_provider',
        ),
    ]
