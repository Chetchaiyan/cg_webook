# Generated by Django 2.1.7 on 2019-04-18 06:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0019_post'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='image',
            field=models.ImageField(null=True, upload_to='post_image'),
        ),
        migrations.AlterField(
            model_name='post',
            name='link',
            field=models.CharField(max_length=256, null=True),
        ),
    ]
