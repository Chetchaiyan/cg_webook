import datetime
import requests

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from requests.auth import HTTPBasicAuth
from rest_framework.authtoken.models import Token

from chet_util.mailgun import Mailgun

MAILGUN_KEY = 'key-7ea75e5848937c78327d81f217ef12bd'
MAILGUN_DOMAIN = 'auto-mail.codegears.biz'
MAILGUN_SENDER = 'AutoMail-WeBook<system@auto-mail.codegears.biz>'

PRICE_DICT = [
    { 'id': '0-1', 'min_kg': 0, 'max_kg': 1, 'min_lb': 0, 'max_lb': 2.2, 'price_usd': 2.84 },
    { 'id': '1-3', 'min_kg': 1, 'max_kg': 3, 'min_lb': 2.2, 'max_lb': 6.6, 'price_usd': 5.34 },
    { 'id': '3-5', 'min_kg': 3, 'max_kg': 5, 'min_lb': 6.6, 'max_lb': 11.0, 'price_usd': 6.67 },
    { 'id': '5-7', 'min_kg': 5, 'max_kg': 7, 'min_lb': 11.0, 'max_lb': 15.4, 'price_usd': 8.00 },
    { 'id': '7-9', 'min_kg': 7, 'max_kg': 9, 'min_lb': 15.4, 'max_lb': 19.8, 'price_usd': 9.34 },
    { 'id': '9-11', 'min_kg': 9, 'max_kg': 11, 'min_lb': 19.8, 'max_lb': 24.2, 'price_usd': 10.00 },
    { 'id': '11-15', 'min_kg': 11, 'max_kg': 15, 'min_lb': 24.2, 'max_lb': 33.1, 'price_usd': 10.67 },
    { 'id': '15-20', 'min_kg': 15, 'max_kg': 20, 'min_lb': 33.1, 'max_lb': 44.1, 'price_usd': 11.34 },
]

class ThailandPostShipping:
    def __init__(self, po_item):
        self.po_item = po_item

    def calculate_checksum(self, code):
        digit = code[2:10]
        weight = "86423597"
        sum = 0
        for i, w in zip(digit, weight) :
            sum += int(i) * int(w)
        remainder = sum % 11
        if remainder == 0 :
            checksum = "5"
        elif remainder == 1 :
            checksum = "0"
        else :
            checksum = str(11 - remainder)
        return code.replace('x', checksum)

    # https://r_dservice.thailandpost.com/webservice/getHistoryStatus?barcode=EB215525005TH
    def create_shipment(self):
        query_barcode = ThaipostBarcode.objects.filter(status='ready')
        if query_barcode.count() == 0 :
            return False
        barcode = query_barcode[0]
        final_code = self.calculate_checksum(barcode.barcode)
        self.po_item.tracking_code = final_code
        self.po_item.save()
        saler = WeBookUser.objects.get(pk=self.po_item.item.user)
        buyer = WeBookUser.objects.get(pk=self.po_item.purchase_order.user)
        # ToDo : get correct data before send to Thaipost
        data_json = {
            "orderId": str(self.po_item.id),
            "invNo": str(self.po_item.id),
            "barcode": final_code,
            "shipperName": saler.fullname,
            "shipperAddress": saler.address,
            "shipperDistrict": "xxx ",
            "shipperProvince": "yyy",
            "shipperZipcode": "10500",
            "shipperEmail": saler.user.email,
            "shipperMobile": saler.phone_number,
            "cusName": buyer.fullname,
            "cusAdd": self.po_item.purchase_order.shipping_address,
            "cusAmp": "xxx",
            "cusProv": "yyy",
            "cusZipcode": "10500",
            "cusTel": buyer.phone_number,
            "productPrice": "0",
            "productInbox": "-",
            "productWeight": "0",
            "orderType": "D",
            # "manifestNo": "TPD20160309-0001",
            # "merchantId": str(saler.user.id),
            # "merchantZipcode": "10120"
        }
        r = requests.post('https://r_dservice.thailandpost.com/webservice/addItem', json=data_json,
                    auth=HTTPBasicAuth('webook', '12345'), verify=False)
        print( r.content )
        if r.status_code != 200 :
            return False
        barcode.status = 'used'
        barcode.save()
        mailgun = Mailgun(MAILGUN_KEY, MAILGUN_DOMAIN, MAILGUN_SENDER)
        email = self.po_item.item.user.email
        r = requests.get('https://r_dservice.thailandpost.com/webservice/LabelPDF?barcode='+final_code,
                    auth=HTTPBasicAuth('webook', '12345'), verify=False)
        mailgun.send_message([email], bcc=['chetchaiyan@gmail.com'],
                subject='[WeBook] thailandpost shippment label',
                text='your book has been purchased.',
                attachment=['thailandpost_label.pdf', r.content, 'application/pdf', {}] )
        return True

class USPSShipping:
    def __init__(self, po_item):
        self.po_item = po_item

    def create_shipment(self):
        # pick 1 barcode and assign to po_item
        # call thaipost to create shipment
        return True

class EmailBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(email=username)
        except UserModel.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None

class WeBookUserToken(Token):
    pass

@receiver(post_save, sender=get_user_model())
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        WeBookUserToken.objects.create(user=instance)

# value, type, period, usable_constrain, count usable
USER_BONUS_DATADICT = [
    [
        (5, 'announcement', 30, False, 0),
    ],
    [
        (10, 'announcement', 60, False, 0),
        (10, 'shipping', 60, True, 9),
    ],
    [
        (15, 'announcement', 120, False, 0),
        (15, 'shipping', 120, True, 7),
    ],
    [
        (100, 'announcement', 180, True, 1),
        (20, 'shipping', 180, True, 10),
    ],
]
# Create your models here.
class WeBookUser(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, primary_key=True)
    create_dt = models.DateTimeField(auto_now_add=True)
    fullname = models.CharField(max_length=128, null=True)
    gender = models.CharField(max_length=24, null=True)
    birthday = models.DateField(null=True)
    education = models.CharField(max_length=128, null=True, default='', blank=True)
    country_code = models.CharField(max_length=24, null=True)
    phone_number = models.CharField(max_length=24, null=True)
    mood_status = models.CharField(max_length=512, null=True, default='', blank=True)
    headline_status = models.CharField(max_length=512, null=True, default='', blank=True)
    address = models.CharField(max_length=512, null=True)
    zipcode = models.CharField(max_length=16, null=True)
    country = models.CharField(max_length=32, null=True)
    province = models.CharField(max_length=32, null=True)
    district = models.CharField(max_length=32, null=True)
    paypal_email = models.CharField(max_length=128, null=True, default='', blank=True)
    photo = models.ImageField(upload_to='webook_user_photo', null=True)
    friend = models.ManyToManyField("WeBookUser", related_name='friend_user')
    friend_blocking = models.ManyToManyField("WeBookUser", related_name='friend_b')
    friend_requesting = models.ManyToManyField("WeBookUser", related_name='friend_r')
    favorite_book = models.ManyToManyField("Book")
    announcement_credit = models.IntegerField(default=0)
    announcement_donate_credit = models.IntegerField(default=0)
    shipping_credit = models.IntegerField(default=0)
    con_allow_friend_request = models.BooleanField(default=True)
    con_allow_other_to_see_friend = models.BooleanField(default=True)
    con_p_noti_direct_message = models.BooleanField(default=True)
    con_p_noti_friend_request = models.BooleanField(default=True)
    con_p_noti_post_like_comment = models.BooleanField(default=True)
    con_p_noti_announcement = models.BooleanField(default=True)
    con_p_noti_ship = models.BooleanField(default=True)
    con_p_noti_save_book = models.BooleanField(default=True)
    con_ia_noti_direct_message = models.BooleanField(default=True)
    con_ia_noti_friend_request = models.BooleanField(default=True)
    con_ia_noti_post_like_comment = models.BooleanField(default=True)
    recommend = models.IntegerField(default=0)
    level = models.IntegerField(default=0)
    rating = models.FloatField(default=0.0)
    coin = models.IntegerField(default=0)
    social_fb_id = models.CharField(max_length=128, null=True)
    social_fb_token = models.CharField(max_length=512, null=True)
    social_line_id = models.CharField(max_length=128, null=True)
    social_line_token = models.CharField(max_length=512, null=True)
    social_google_id = models.CharField(max_length=128, null=True)
    social_google_token = models.CharField(max_length=2048, null=True)
    current_lat = models.DecimalField(max_digits=10, decimal_places=7, default=0.0)
    current_long = models.DecimalField(max_digits=10, decimal_places=7, default=0.0)
    # ToDo : link to facebook account, line account, google account

    class Meta:
        indexes = [
            models.Index(fields=['social_fb_id']),
            models.Index(fields=['social_line_id']),
            models.Index(fields=['social_google_id']),
            models.Index(fields=['create_dt']),
            models.Index(fields=['recommend']),
        ]

    def level_up(self, level):
        self.level = level
        bonus_level = USER_BONUS_DATADICT[level-1]
        for b in bonus_level :
            u_bonus = UserBonus(user=self.user, bonus_type=b[1], bonus_value=b[0],
                        usable_constrain=b[3], usable_remain=b[4])
            u_bonus.expire_date = datetime.date.today() + datetime.timedelta(days=b[2])
            u_bonus.save()

    def level_check(self):
        rating_count = self.user.target.filter(rating=5).count()
        send_count = self.user.owner.filter(store_status='removed').count()
        if self.level == 0 :
            if rating_count >= 5 or send_count >= 5 :
                self.level_up(1)
        elif self.level == 1 :
            if rating_count >= 15 or send_count >= 15 :
                self.level_up(2)
        elif self.level == 2 :
            if rating_count >= 25 or send_count >= 25 :
                self.level_up(3)
        elif self.level == 3 :
            if rating_count >= 50 or send_count >= 50 :
                self.level_up(4)

    def update_recommend(self):
        review_list = self.user.target.all().order_by("-create_dt")[:30].values_list('rating', flat=True)
        review_count = review_list.count()
        book_submit = self.user.owner.all().count()
        self.recommend = (self.level * 50) + (self.rating * review_count) + (book_submit * 10)

    def save(self, *args, **kwargs):
        # ToDo : calculate recommend / rating / level
        review_list = self.user.target.all().order_by("-create_dt")[:30].values_list('rating', flat=True)
        review_count = review_list.count()
        self.rating = 0 if review_count == 0 else sum( review_list ) / review_count
        self.level_check()
        self.update_recommend()
        super().save(*args, **kwargs)

    def check_blocking_status(self, target_user):
        if target_user in self.friend_blocking.all() or self in target_user.friend_blocking.all() :
            return True
        return False

    @property
    def rating_display(self):
        five = UserRating.objects.filter(target=self.user, rating=5).count()
        four = UserRating.objects.filter(target=self.user, rating=4).count()
        three = UserRating.objects.filter(target=self.user, rating=3).count()
        two = UserRating.objects.filter(target=self.user, rating=2).count()
        one = UserRating.objects.filter(target=self.user, rating=1).count()
        return [five, four, three, two, one]

    @property
    def user_bonus(self):
        return self.user.user_bonus

class UserBonus(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='user_bonus')
    create_dt = models.DateTimeField(auto_now_add=True)
    bonus_type = models.CharField(max_length=128)
    bonus_value = models.IntegerField(default=0)
    expire_date = models.DateField()
    usable_constrain = models.BooleanField(default=False)
    usable_remain = models.IntegerField(default=0)

class UserRating(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='rater')
    target = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='target')
    create_dt = models.DateTimeField(auto_now_add=True)
    rating = models.IntegerField()

class PrivateMessageManager(models.Manager):

    def get_related(self, user_1, user_2):
        return super().get_queryset().filter( (Q(sender=user_1) & Q(receiver=user_2)) |
                    (Q(sender=user_2) & Q(receiver=user_1)) ).order_by('-create_dt')

class PrivateMessage(models.Model):
    sender = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='receiver')
    message = models.CharField(max_length=1024)
    create_dt = models.DateTimeField(auto_now_add=True)
    # ToDo : how to display latest message effectively
    objects = PrivateMessageManager()
    newest = models.BooleanField(default=True)

    class Meta:
        indexes = [
            models.Index(fields=['create_dt']),
            models.Index(fields=['sender', 'create_dt']),
            models.Index(fields=['receiver', 'create_dt']),
        ]

    @property
    def sender_name(self):
        return self.sender.webookuser.fullname

    @property
    def receiver_name(self):
        return self.receiver.webookuser.fullname

class ImagePool(models.Model):
    object_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    object = GenericForeignKey('object_type', 'object_id')
    image = models.ImageField(upload_to='image_pool', null=True)

    @property
    def object_class(self):
        return self.object.__class__.__name__.lower() if self.object else 'None'

class ShippingProvider(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name

class BookCategory3(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name

def default_category():
    return BookCategory.objects.all()[0]

STORE_STATUS = [
    ('on_shelf', 'on_shelf'),
    ('removed', 'removed'),
]

class Book(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='owner')
    create_dt = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=512)
    condition = models.CharField(max_length=128)
    category = models.ForeignKey(BookCategory3, on_delete=models.SET(default_category), null=True)
    author = models.CharField(max_length=128)
    publication_year = models.IntegerField()
    book_format = models.CharField(max_length=128)
    language = models.CharField(max_length=128)
    geo_lat = models.DecimalField(max_digits=10, decimal_places=7)
    geo_long = models.DecimalField(max_digits=10, decimal_places=7)
    weight = models.CharField(max_length=128)
    owner_comment = models.CharField(max_length=2048, null=True)
    image = GenericRelation(ImagePool, content_type_field='object_type', object_id_field='object_id')
    rating = models.FloatField(default=0.0)
    review_count = models.IntegerField(default=0)
    shipping_provider = models.ManyToManyField("ShippingProvider")
    store_status = models.CharField(max_length=128, default="on_shelf", choices=STORE_STATUS)

    class Meta:
        indexes = [
            models.Index(fields=['create_dt']),
            models.Index(fields=['title']),
            models.Index(fields=['rating']),
            models.Index(fields=['category']),
            models.Index(fields=['review_count']),
            models.Index(fields=['author']),
            models.Index(fields=['publication_year']),
        ]

    @property
    def webook_user(self):
        return WeBookUser.objects.get(pk=self.user)

    @property
    def price(self, shipping_provider=None):
        price_list = {}
        price_thailand_post = [ i for i in PRICE_DICT if i['id'] == self.weight ]
        if price_thailand_post :
            price_list['ThailandPost'] = price_thailand_post[0]['price_usd']
        else :
            price_list['ThailandPost'] = PRICE_DICT[0]['price_usd']
        return price_list

    def exact_price(self, shipping_provider):
        price_list = self.price
        if shipping_provider in price_list :
            return price_list[shipping_provider]
        else :
            print("Error: Shipping Provider not found - main_app.models.Book")
            return 9.99

class BookReview(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    create_dt = models.DateTimeField(auto_now_add=True)
    review_text = models.CharField(max_length=2048)
    rating = models.IntegerField()

    class Meta:
        unique_together = (("user", "book"),)
        indexes = [
            models.Index(fields=['create_dt']),
            models.Index(fields=['rating']),
        ]

    @property
    def webook_user(self):
        return WeBookUser.objects.get(pk=self.user)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        book = self.book
        review_list = book.bookreview_set.all().order_by("-create_dt")[:30].values_list('rating', flat=True)
        review_count = review_list.count()
        book.rating = 0 if review_count == 0 else sum( review_list ) / review_count
        book.review_count = book.bookreview_set.all().count()
        book.save()

class Post(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    create_dt = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=256, null=True, blank=True)
    post_text = models.CharField(max_length=2048)
    image = models.ImageField(upload_to='post_image', null=True)
    post_exposure = models.CharField(max_length=24) # public , anonymous, friend, announcement, announcement_donate
    link = models.CharField(max_length=256, null=True, blank=True)
    trend = models.IntegerField(default=100)
    is_active = models.BooleanField(default=True)
    geo_lat = models.DecimalField(max_digits=10, decimal_places=7, default=0.0)
    geo_long = models.DecimalField(max_digits=10, decimal_places=7, default=0.0)
    is_geo_constrain = models.BooleanField(default=False)

    class Meta:
        indexes = [
            models.Index(fields=['create_dt']),
            models.Index(fields=['post_exposure', 'create_dt']),
        ]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    @property
    def comment_count(self):
        return self.postcomment_set.count()

    @property
    def like(self):
        return self.postlike_set.count()

    @property
    def webook_user(self):
        return WeBookUser.objects.get(pk=self.user)

class PostLike(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("user", "post"),)

class PostNotification(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("user", "post"),)

class PostDonate(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("user", "post"),)

class PostComment(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    create_dt = models.DateTimeField(auto_now_add=True)
    comment_text = models.CharField(max_length=2048)

    @property
    def webook_user(self):
        return WeBookUser.objects.get(pk=self.user)

class Report(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    create_dt = models.DateTimeField(auto_now_add=True)
    report_text = models.CharField(max_length=2048)
    object_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    object = GenericForeignKey('object_type', 'object_id')
    image = GenericRelation(ImagePool, content_type_field='object_type', object_id_field='object_id')

    @property
    def object_class(self):
        return self.object.__class__.__name__.lower() if self.object else 'None'

class PurchaseOrder(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    create_dt = models.DateTimeField(auto_now_add=True)
    payment = models.CharField(max_length=128, null=True)
    status = models.CharField(max_length=128, default="not_paid")
    shipping_address = models.CharField(max_length=1024)
    shipping_zipcode = models.CharField(max_length=16, null=True)
    shipping_country = models.CharField(max_length=32, null=True)
    shipping_province = models.CharField(max_length=32, null=True)
    shipping_district = models.CharField(max_length=32, null=True)

    class Meta:
        indexes = [
            models.Index(fields=['create_dt']),
        ]

    def paid(self, payment=None):
        self.payment = payment
        self.status = 'shipping'
        po_item = self.po_item.all()
        for item in po_item :
            item.shipment_status = 'shipping'
            item.item.store_status = 'remove'
            item.item.save()
            item.save()
            shipping = ThailandPostShipping(item)
            if item.shipping_provider is not None :
                if item.shipping_provider.name == 'ThailandPost' :
                    shipping = ThailandPostShipping(item)
                elif item.shipping_provider.name == 'USPS' :
                    shipping = USPSShipping(item)
                else :
                    # shipping not found
                    pass
            else :
                shipping = ThailandPostShipping(item)
            result = shipping.create_shipment()
            # ToDo : if fail cancel order
            if not result :
                pass
        self.save()

    @property
    def total_amount(self):
        po_item = self.po_item.all()
        total_amount = 0.0
        for item in po_item :
            total_amount += float(item.price)
        return total_amount

class PurchaseOrderItem(models.Model):
    purchase_order = models.ForeignKey(PurchaseOrder, on_delete=models.CASCADE, related_name="po_item")
    create_dt = models.DateTimeField(auto_now_add=True)
    item = models.ForeignKey(Book, on_delete=models.CASCADE, null=True)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    shipping_provider = models.ForeignKey(ShippingProvider, on_delete=models.CASCADE, null=True)
    shipment_status = models.CharField(max_length=128, default="packing")
    is_rated = models.BooleanField(default=False)
    tracking_code = models.CharField(max_length=32, default='')

    @property
    def webook_user(self):
        return WeBookUser.objects.get(pk=self.user)

    @property
    def book_detail(self):
        return self.item

BARCODE_STATUS = [
    ('ready', 'ready'),
    ('used', 'used'),
]

# status = ready, used
class ThaipostBarcode(models.Model):
    barcode = models.CharField(max_length=16)
    status = models.CharField(max_length=128, default='ready', choices=BARCODE_STATUS)

class PaymentTransaction(models.Model):
    create_dt = models.DateTimeField(auto_now_add=True)
    paid_by = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    payment_category = models.CharField(max_length=32, null=True)
    payment_gateway = models.CharField(max_length=32, null=True)
    payment_amount = models.DecimalField(max_digits=6, decimal_places=2)
    payment_reference = models.CharField(max_length=32, null=True)
    transaction_data = models.CharField(max_length=2048, null=True)
