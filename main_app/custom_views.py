from django.http import HttpResponse
from django.template import Template, Context
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model

from requests.auth import HTTPBasicAuth
import requests
import datetime

import logging as logging_dg
logger_dg = logging_dg.getLogger('django.error')

'''
from google.cloud import logging
logging_client = logging.Client()
log_name = 'my-log'
logger = logging_client.logger(log_name)
'''

import json
from main_app.models import *

PAYPAL_HTML = """
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    </head>
    <body>
        <script src="https://www.paypal.com/sdk/js?client-id=AVB1hhvWrCrPzCr5106bLeAs4jhUeI4xrXxYkgeg7gmjABxy2CLnAqCM5RQ7jfq7vYMHmnuailReP99l"></script>
        <div id="paypal-button-container"></div>
        <script>
            paypal.Buttons({
                createOrder: function(data, actions) {
                    return actions.order.create({ purchase_units: [
                        {
                            amount: {
                                value: {{total_amount}},
                                {% if items %}
                                    breakdown: { item_total: { currency_code: 'USD', value: {{total_amount}} } }
                                {% endif %}
                            },
                            reference_id: '{{reference_id}}',
                            {% if items %}
                                items: {{items|safe}}
                            {% endif %}
                        }
                    ]});
                },
                onApprove: function(data, actions) {
                    return actions.order.capture().then(function(details) {
                        var order_id = data['orderID'];
                        var payer_id = data['payerID'];
                        var reference_id = details['purchase_units'][0]['reference_id'];
                        window.location.assign('/custom/paypal_transaction_complete?order_id='+order_id+'&payer_id='+payer_id+'&reference_id='+reference_id);
                    });
                }
            }).render('#paypal-button-container');
      </script>
    </body>
</html>
"""

''' NOTE :
=== console.log(JSON.stringify(data)); ===
{"orderID":"1J7209289P8141045","payerID":"XX4QSK2N6Z4KJ"}

=== console.log(JSON.stringify(details)); ===
{"create_time":"2019-06-14T04:53:57Z","update_time":"2019-06-14T04:53:57Z","id":"1J7209289P8141045","intent":"CAPTURE","status":"COMPLETED","payer":{"email_address":"webook_test_1@gmail.com","payer_id":"XX4QSK2N6Z4KJ","address":{"address_line_1":"1 Main St","admin_area_2":"San Jose","admin_area_1":"CA","postal_code":"95131","country_code":"US"},"name":{"given_name":"Chet","surname":"Chetchaiyan"},"phone":{"phone_number":{"national_number":"4083091798"}}},"purchase_units":[{"description":"Test Book 1","reference_id":"default","custom_id":"5","amount":{"value":"19.98","currency_code":"USD","breakdown":{"item_total":{"value":"19.98","currency_code":"USD"},"shipping":{"value":"0.00","currency_code":"USD"},"handling":{"value":"0.00","currency_code":"USD"},"insurance":{"value":"0.00","currency_code":"USD"},"shipping_discount":{"value":"0.00","currency_code":"USD"}}},"payee":{"email_address":"barco.03-facilitator@gmail.com","merchant_id":"YQZCHTGHUK5P8"},"items":[{"name":"Test Book 1","unit_amount":{"value":"9.99","currency_code":"USD"},"tax":{"value":"0.00","currency_code":"USD"},"quantity":"1"},{"name":"Test Book 2","unit_amount":{"value":"9.99","currency_code":"USD"},"tax":{"value":"0.00","currency_code":"USD"},"quantity":"1"}],"shipping":{"name":{"full_name":"Chet Chetchaiyan"},"address":{"address_line_1":"1 Main St","admin_area_2":"San Jose","admin_area_1":"CA","postal_code":"95131","country_code":"US"}},"payments":{"captures":[{"status":"COMPLETED","id":"0SF63795VS5930209","final_capture":true,"create_time":"2019-06-14T04:53:57Z","update_time":"2019-06-14T04:53:57Z","amount":{"value":"19.98","currency_code":"USD"},"seller_protection":{"status":"ELIGIBLE","dispute_categories":["ITEM_NOT_RECEIVED","UNAUTHORIZED_TRANSACTION"]},"links":[{"href":"https://api.sandbox.paypal.com/v2/payments/captures/0SF63795VS5930209","rel":"self","method":"GET","title":"GET"},{"href":"https://api.sandbox.paypal.com/v2/payments/captures/0SF63795VS5930209/refund","rel":"refund","method":"POST","title":"POST"},{"href":"https://api.sandbox.paypal.com/v2/checkout/orders/1J7209289P8141045","rel":"up","method":"GET","title":"GET"}]}]}}],"links":[{"href":"https://api.sandbox.paypal.com/v2/checkout/orders/1J7209289P8141045","rel":"self","method":"GET","title":"GET"}]}
'''

ANNOUNCEMENT_PRICE = 2.0
ANNOUNCEMENT_DONATE_PRICE = 5.0
@csrf_exempt
def paypal(request):
    if request.method == 'GET':
        if 'po' in request.GET :
            purchase_order = PurchaseOrder.objects.get(pk=request.GET['po'])
            po_item = purchase_order.po_item.all()
            item_list = []
            total_amount = 0.0
            for item in po_item :
                i = {
                    'name' : item.item.title,
                    'quantity' : 1,
                    'unit_amount' : { 'currency_code' : 'USD', 'value': float(item.price) }
                }
                total_amount += float(item.price)
                item_list.append( i )
            context = Context({'items': item_list, 'total_amount': total_amount, 'reference_id': request.GET['po'] })
        elif 'announcement_user_id' in request.GET :
            t = datetime.datetime.today().strftime("%Y%m%d_%H%M")
            if request.GET['announcement_type'] == 'normal' :
                total_amount = ANNOUNCEMENT_PRICE
            elif request.GET['announcement_type'] == 'donate' :
                total_amount = ANNOUNCEMENT_DONATE_PRICE
            else :
                total_amount = ANNOUNCEMENT_PRICE
            type = 'n' if request.GET['announcement_type'] == 'normal' else 'd'
            reference_id = 'an_' + type + '_' + t + '_' + request.GET['announcement_user_id']
            context = Context({'total_amount': total_amount, 'reference_id': reference_id })
        elif 'donate_amount' in request.GET :
            user = get_user_model().objects.get(pk=request.GET['user_id'])
            post = Post.objects.get(pk=request.GET['post_id'])
            post_donate = PostDonate(user=user, post=post)
            post_donate.save()
            t = datetime.datetime.today().strftime("%Y%m%d_%H%M")
            reference_id = 'do_' + t + '_' + request.GET['user_id'] + '_' + request.GET['receipent_id']
            context = Context({'total_amount': request.GET['donate_amount'], 'reference_id': reference_id })
    template = Template(PAYPAL_HTML)
    return HttpResponse(template.render(context))

PAYPAL_CLIENT = 'AVB1hhvWrCrPzCr5106bLeAs4jhUeI4xrXxYkgeg7gmjABxy2CLnAqCM5RQ7jfq7vYMHmnuailReP99l'
PAYPAL_SECERT = 'EH91NTgnO8FVTvyPTHpC-tVVB0kiRzNuxfXPvYESjSFIx6RyWAz8_NQekUWiQcTViY57UKJAR_CBY9s4'
PAYPAL_ORDER_API = 'https://api.sandbox.paypal.com/v2/checkout/orders/'

def paypal_transaction_complete(request):
    '''
    logger.log_text(request.GET)
    logger.log_text(request.GET['order_id'])
    '''

    try:
        order_response = requests.get(PAYPAL_ORDER_API+request.GET['order_id'],
                auth=HTTPBasicAuth(PAYPAL_CLIENT, PAYPAL_SECERT) )
    except requests.exceptions.RequestException as e:
        logger_dg.error('requests_paypal: ' + e)

    logger_dg.error('response_paypal: ')
    logger_dg.error(order_response)

    try:
        order_response_dict = json.loads(order_response.content.decode('utf-8'))
    except Exception as e :
        logger_dg.error(e)
        logger_dg.error(order_response.content)

    reference_id = order_response_dict['purchase_units'][0]['reference_id']
    if reference_id.startswith('an_') :
        split_str = reference_id.split('_')
        user_id = split_str[-1]
        user = get_user_model().objects.get(pk=user_id)
        webook_user = WeBookUser.objects.get(pk=user)
        announcement_type = split_str[1]
        if announcement_type == 'n' :
            webook_user.announcement_credit += 1
        elif announcement_type == 'd' :
            webook_user.announcement_donate_credit += 1
        webook_user.save()
        transaction = PaymentTransaction(paid_by=user, payment_category='announcement', payment_gateway='paypal',
                    payment_amount=float(order_response_dict['purchase_units'][0]['amount']['value']),
                    payment_reference=order_response_dict['id'], transaction_data=order_response.content )
        transaction.save()
    elif reference_id.startswith('do_') :
        split_str = reference_id.split('_')
        user_id = split_str[-2]
        user = get_user_model().objects.get(pk=user_id)
        receipent_id = split_str[-1]
        transaction = PaymentTransaction(paid_by=user, payment_category='donate', payment_gateway='paypal',
                    payment_amount=float(order_response_dict['purchase_units'][0]['amount']['value']),
                    payment_reference=order_response_dict['id'], transaction_data=order_response.content )
        transaction.save()
    else :
        po = PurchaseOrder.objects.get(pk=order_response_dict['purchase_units'][0]['reference_id'])
        po.paid('paypal')
        po.save()
        transaction = PaymentTransaction(paid_by=po.user, payment_category='book_purchase', payment_gateway='paypal',
                    payment_amount=float(order_response_dict['purchase_units'][0]['amount']['value']),
                    payment_reference=order_response_dict['id'], transaction_data=order_response.content )
        transaction.save()
    return HttpResponse("Completed!!")

def thaipost_tracking_bridge(request):
    barcode = request.GET['barcode']
    response = requests.get('https://r_dservice.thailandpost.com/webservice/getHistoryStatus?barcode='+barcode,
                auth=HTTPBasicAuth('webook', '12345'), verify=False)
    print(response.content)
    return HttpResponse(response.content)

'''
// Note: This code is intended as a *pseudocode* example. Each server platform and programming language has a different way of handling requests, making HTTP API calls, and serving responses to the browser.

// 1. Set up your server to make calls to PayPal

// 1a. Add your client ID and secret
PAYPAL_CLIENT = 'AVB1hhvWrCrPzCr5106bLeAs4jhUeI4xrXxYkgeg7gmjABxy2CLnAqCM5RQ7jfq7vYMHmnuailReP99l';
PAYPAL_SECRET = 'EH91NTgnO8FVTvyPTHpC-tVVB0kiRzNuxfXPvYESjSFIx6RyWAz8_NQekUWiQcTViY57UKJAR_CBY9s4';

// 1b. Point your server to the PayPal API
PAYPAL_OAUTH_API = 'https://api.sandbox.paypal.com/v1/oauth2/token/';
PAYPAL_ORDER_API = 'https://api.sandbox.paypal.com/v2/checkout/orders/';

// 1c. Get an access token from the PayPal API
basicAuth = base64encode(`${ PAYPAL_CLIENT }:${ PAYPAL_SECRET }`);
auth = http.post(PAYPAL_OAUTH_API {
  headers: {
    Accept:        `application/json`,
    Authorization: `Basic ${ basicAuth }`
  },
  data: `grant_type=client_credentials`
});

// 2. Set up your server to receive a call from the client
function handleRequest(request, response) {

  // 2a. Get the order ID from the request body
  orderID = request.body.orderID;

  // 3. Call PayPal to get the transaction details
  details = http.get(PAYPAL_ORDER_API + orderID, {
    headers: {
      Accept:        `application/json`,
      Authorization: `Bearer ${ auth.access_token }`
    }
  });

  // 4. Handle any errors from the call
  if (details.error) {
    return response.send(500);
  }

  // 5. Validate the transaction details are as expected
  if (details.purchase_units[0].amount.value !== '5.77') {
    return response.send(400);
  }

  // 6. Save the transaction in your database
  database.saveTransaction(orderID);

  // 7. Return a successful response to the client
  return response.send(200);
}
'''
