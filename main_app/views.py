import json
import random
import string
import datetime

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User, Group
from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.db.models import Q

from rest_framework import filters
from rest_framework import generics
from rest_framework import permissions
from rest_framework import viewsets, mixins
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action, schema
from rest_framework.response import Response
from rest_framework.schemas import AutoSchema
from rest_framework.views import APIView
from rest_framework import pagination

from django_filters.rest_framework import DjangoFilterBackend
from allauth.account.forms import SignupForm

from .serializers import *
from .models import WeBookUser, PrivateMessage, Book, BookReview, ShippingProvider, PRICE_DICT

from chet_util import push_gears
from chet_util.mailgun import Mailgun

HARD_CAP = 1000

MAILGUN_KEY = 'key-7ea75e5848937c78327d81f217ef12bd'
MAILGUN_DOMAIN = 'auto-mail.codegears.biz'
MAILGUN_SENDER = 'AutoMail-WeBook<system@auto-mail.codegears.biz>'

class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `owner`.
        return obj.user == request.user

class IsObjectOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.object.user == request.user

class WeBookUserFriendRelationFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        friend_relation = request.query_params.get('friend_relation', None)
        if friend_relation is not None :
            webook_user = WeBookUser.objects.get(pk=request.user)
            if friend_relation == 'friend' :
                return webook_user.friend.all()
            elif friend_relation == 'friend_blocking' :
                return webook_user.friend_blocking.all()
            elif friend_relation == 'friend_requesting' :
                return webook_user.friend_requesting.all()
            elif friend_relation == 'friend_request' :
                return webook_user.friend_r.all()
            else :
                return queryset
        return queryset

class WeBookUserViewSet(mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = WeBookUser.objects.all()
    serializer_class = WeBookUserSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (WeBookUserFriendRelationFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    ordering_fields = ('create_dt', 'recommend')
    ordering = ('-create_dt', )
    search_fields = ('fullname', 'headline_status', 'address',)

    ''' ToDo : update user level, recommend oppon request
    def retrieve(self, request, pk=None):
        pass
    '''

    @action(methods=['get'], detail=False)
    def me(self, request, format=None):
        webook_user = WeBookUserSerializer(WeBookUser.objects.get(pk=request.user), context={'request': request})
        return Response( webook_user.data )

    @action(methods=['post'], detail=False, permission_classes=[permissions.AllowAny])
    def register(self, request, format=None):
        """
        post:
        New user registration by using username and password
        """
        form = SignupForm(request.data)
        if form.is_valid():
            user = form.save(request)
            webook_user = WeBookUser(user=user)
            for attr, value in request.data.items():
                setattr(webook_user, attr, value)
            webook_user.coin = 65
            webook_user.save()
            return Response( WeBookUserSerializer(webook_user, context={'request': request}).data )
        else :
            return Response({"detail": form.errors.as_data()}, status=400)

    @action(methods=['post'], detail=False, permission_classes=[permissions.AllowAny])
    def social_login(self, request, format=None):
        social = request.data['social']
        social_id = request.data['id']
        if social == 'fb' :
            query_webook_user = WeBookUser.objects.filter(social_fb_id=social_id)
        elif social == 'line':
            query_webook_user = WeBookUser.objects.filter(social_line_id=social_id)
        elif social == 'google':
            query_webook_user = WeBookUser.objects.filter(social_google_id=social_id)
        if query_webook_user :
            webook_user = query_webook_user.get()
            token = Token.objects.get(user=webook_user.user)
            return Response({"token": token.key}, status=200)
        else :
            return Response({"detail": "user not found."}, status=400)

    # ToDo : Limit Photo size upload
    @action(methods=['post', 'get'], detail=True, permission_classes=[permissions.AllowAny])
    def photo(self, request, pk=None):
        webook_user = get_object_or_404(self.queryset, pk=pk)
        if request.method == 'GET':
            if webook_user.photo :
                response = HttpResponse(webook_user.photo, content_type="image/jpeg")
                # response['Content-Disposition'] = 'attachment; filename="{}"'.format(webook_user.photo.name)
                return response
            else :
                return Response({"detail": "No Photo."}, status=200)
        elif request.method == 'POST':
            photo = request.FILES['photo']
            webook_user.photo = photo
            webook_user.save()
            return Response({"detail": "Upload Success."}, status=200)

    @action(methods=['post'], detail=False)
    def add_friend(self, request, format=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        webook_user.friend_requesting.add(request.data['friend'])
        webook_user.save()
        d = {
            'user_id' : request.user.id,
            'fullname' : webook_user.fullname,
            'timestamp' : datetime.datetime.now().isoformat(),
            'noti_type' : 'friend_request',
        }
        push_gears.push_target(request.data['friend'], "Friend Request", data=d, badge_count=True)
        return Response({"detail": "Add Friend Success."}, status=200)

    @action(methods=['get'], detail=True)
    def friend_of_friend(self, request, pk=None):
        webook_user = get_object_or_404(self.queryset, pk=pk)
        current_user = WeBookUser.objects.get(pk=request.user)
        webook_user_data = WeBookUserSerializer(webook_user.friend.all(), many=True).data
        current_user_friend = current_user.friend.all()
        current_user_friend_id = [ i.user.id for i in current_user_friend ]
        for idx, u in enumerate(webook_user_data) :
            if u['user']['id'] in current_user_friend_id :
                webook_user_data[idx]['is_friend'] = True
            else :
                webook_user_data[idx]['is_friend'] = False
        return Response(webook_user_data)

    @action(methods=['post'], detail=False)
    def remove_friend(self, request, format=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        webook_user.friend.remove(request.data['friend'])
        webook_user.save()
        return Response({"detail": "Remove Friend Success."}, status=200)

    @action(methods=['post'], detail=False)
    def reject_friend_request(self, request, format=None):
        """
        Rejecting incoming friend request
        """
        webook_user = WeBookUser.objects.get(pk=request.user)
        friend_id = request.data['friend']
        friend_r_list = webook_user.friend_r.all().values_list('user_id', flat=True )
        if friend_id in friend_r_list :
            friend_request = webook_user.friend_r.remove( friend_id )
            webook_user.save()
            return Response({"detail": "Request Rejected Success."}, status=200)
        else :
            return Response({"detail": "Request Rejected Fail."}, status=400)

    @action(methods=['post'], detail=False)
    def accept_friend(self, request, format=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        accept_friend = request.data['friend']
        friend_r_list = webook_user.friend_r.all().values_list('user_id', flat=True)
        if accept_friend not in friend_r_list :
            return Response({"detail": "Friend not requested."}, status=400)
        else :
            webook_user.friend_r.remove(accept_friend)
            webook_user.friend.add(accept_friend)
            user = User.objects.get(pk=accept_friend)
            webook_user_2 = WeBookUser.objects.get(pk=user)
            webook_user_2.friend.add(webook_user.user_id)
            return Response({"detail": "Accept Friend Success."}, status=200)

    @action(methods=['post'], detail=False)
    def block_friend(self, request, format=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        block_friend = request.data['friend']
        friend_list = webook_user.friend.all().values_list('user_id', flat=True)
        if block_friend not in friend_list :
            return Response({"detail": "Can't block people who isn't friend."}, status=400)
        else :
            webook_user.friend.remove(block_friend)
            webook_user.friend_blocking.add(block_friend)
            return Response({"detail": "Block Friend Success."}, status=200)

    @action(methods=['post'], detail=False)
    def unblock_friend(self, request, format=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        unblock_friend = request.data['friend']
        friend_blocking_list = webook_user.friend_blocking.all().values_list('user_id', flat=True)
        if unblock_friend not in friend_blocking_list :
            return Response({"detail": "Can't unblock."}, status=400)
        else :
            webook_user.friend.add(unblock_friend)
            webook_user.friend_blocking.remove(unblock_friend)
            return Response({"detail": "Unblock Friend Success."}, status=200)

    @action(methods=['get'], detail=False)
    def book_favorite(self, request, format=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        return Response( BookSerializer(webook_user.favorite_book.all()[:HARD_CAP],
                    many=True, context={'request': request}).data )

    @action(methods=['post'], detail=False)
    def change_password(self, request, format=None):
        old_password = request.data['old_password']
        new_password = request.data['new_password']
        if request.user.check_password(old_password) :
            request.user.set_password( new_password  )
            request.user.save()
            return Response({"detail": "Change password success."}, status=200)
        else :
            return Response({"detail": "Wrong old password."}, status=400)

    @action(methods=['post'], detail=False, permission_classes=[permissions.AllowAny])
    def forget_password(self, request, format=None):
        username = request.data['username']
        email = request.data['email']
        query_user = get_user_model().objects.all().filter(username=username)
        if query_user.exists() :
            user = query_user.get()
            if user.email == email :
                mailgun = Mailgun(MAILGUN_KEY, MAILGUN_DOMAIN, MAILGUN_SENDER)
                password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
                user.set_password(password)
                user.save()
                mailgun.send_message([email], subject='[WeBook] New Password',
                        text='Yours new password is {}. \nPlease change your after login.'.format(password))
                return Response({"detail": "Forget password success, new password has been send out."}, status=200)
        return Response({"detail": "Username and email don't match with any account."}, status=400)

    @action(methods=['post'], detail=False)
    def coin_to_announcement(self, request, format=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        if webook_user.coin >= 65 :
            webook_user.coin -= 65
            webook_user.announcement_credit += 1
            webook_user.save()
            return Response({"detail": "Success to exchange 65 coins for 1 announcement credit."}, status=200)
        else :
            return Response({"detail": "Coin not enough."}, status=400)

    @action(methods=['post'], detail=False)
    def coin_to_shipping(self, request, format=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        credit = request.data['credit']
        used_coin = credit * 30
        if webook_user.coin >= used_coin :
            webook_user.coin -= used_coin
            webook_user.shipping_credit += credit
            webook_user.save()
            return Response({"detail": "Success to exchange {} coins for {} shipping credit.".format(
                        used_coin, credit)}, status=200)
        else :
            return Response({"detail": "Coin not enough."}, status=400)

    @action(methods=['post'], detail=False)
    def test_add_coin(self, request, format=None):
        if request.data['code'] == 'CodeGears-026363978' :
            webook_user = WeBookUser.objects.get(pk=request.user)
            webook_user.coin += 100
            webook_user.save()
            return Response({"detail": "Coin Added."}, status=200)
        return Response({"detail": "code wrong."}, status=400)

class UserRatingViewSet(viewsets.ModelViewSet):
    queryset = UserRating.objects.all()
    serializer_class = UserRatingSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)

# 1.) Query all message, both send and received group by paticipant(sender or receiver) order by create_dt
# 2.) display message according to prefered user
# 3.) send message
class PrivateMessageViewSet(mixins.ListModelMixin,
                            viewsets.GenericViewSet):
    queryset = PrivateMessage.objects.all()
    serializer_class = PrivateMessageSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend)
    filterset_fields = ('sender', 'receiver', 'newest')
    ordering_fields = ('create_dt')
    ordering = ('-create_dt', )

    def get_queryset(self):
        return PrivateMessage.objects.filter( Q(sender=self.request.user) | Q(receiver=self.request.user)
                    ).order_by('-create_dt')

    @action(methods=['get'], detail=True)
    def read(self, request, pk=None):
        user_1 = request.user
        user_2 = get_user_model().objects.get(pk=pk)
        query_pm = PrivateMessage.objects.get_related(user_1, user_2)
        return Response( PrivateMessageSerializer(query_pm, many=True, context={'request': request}).data )

    @action(methods=['post'], detail=False)
    def send(self, request, format=None):
        receiver = get_user_model().objects.get(pk=request.data['receiver'])
        receiver_user = WeBookUser.objects.get(pk=receiver)
        webook_user = WeBookUser.objects.get(pk=request.user)
        if webook_user.check_blocking_status(receiver_user) :
            return Response( {"detail": "User is blocking or has been blocked."}, status=400 )
        if receiver :
            PrivateMessage.objects.filter(sender=request.user, receiver=receiver, newest=True).update(newest=False)
            PrivateMessage.objects.filter(sender=receiver, receiver=request.user, newest=True).update(newest=False)
            pm = PrivateMessage(sender=request.user, receiver=receiver, message=request.data['message'])
            pm.save()
            d = {
                'user_id' : request.user.id,
                'fullname' : webook_user.fullname,
                'timestamp' : datetime.datetime.now().isoformat(),
                'noti_type' : 'direct_message',
            }
            push_gears.push_target(request.data['receiver'], "Direct Message", data=d, badge_count=True)
            return Response( PrivateMessageSerializer(pm, context={'request': request}).data )
        else :
            return Response( {"detail": "Receiver not found."}, status=404 )

    @action(methods=['post'], detail=False)
    def clear(self, request, format=None):
        user_1 = request.user
        user_2 = get_user_model().objects.get(pk=request.data['conversation_user'])
        query_pm = PrivateMessage.objects.get_related(user_1, user_2)
        query_pm.delete()
        return Response( {"detail": "Clear PrivateMessage Completed."}, status=200 )

class ShippingProviderViewSet(viewsets.ModelViewSet):
    queryset = ShippingProvider.objects.all()
    serializer_class = ShippingProviderSerializer
    permission_Classes = (permissions.IsAuthenticated, )

class LargeResultsSetPagination(pagination.PageNumberPagination):
    page_size = 20

class BookCategoryViewSet(viewsets.ModelViewSet):
    queryset = BookCategory3.objects.all()
    serializer_class = BookCategorySerializer
    permission_Classes = (permissions.IsAuthenticated, )
    pagination_class = LargeResultsSetPagination

class BookClosestFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        closest =request.query_params.get('closest', None)
        if closest :
            webook_user = WeBookUser.objects.get(pk=request.user)
            return queryset.filter(user__webookuser__country=webook_user.country)
        else :
            return queryset

class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend, BookClosestFilterBackend)
    filterset_fields = ('user', 'store_status')
    ordering_fields = ('title', 'create_dt', 'rating', 'category', 'review_count')
    ordering = ('-create_dt', )
    search_fields = ('title', 'author', 'category__name', 'language', 'publication_year', 'owner_comment', )

    @action(methods=['post'], detail=True)
    def add_favorite(self, request, pk=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        webook_user.favorite_book.add(pk)
        book = Book.objects.get(pk=pk)
        webook_user.save()
        d = {
            'user_id' : request.user.id,
            'book' : pk,
            'fullname' : webook_user.fullname,
            'timestamp' : datetime.datetime.now().isoformat(),
            'noti_type' : 'book_favorite',
        }
        if book.user != request.user :
            push_gears.push_target(book.user.id, "Book Favorite", data=d, badge_count=True)
        return Response({"detail": "Add Favorite Book Success."}, status=200)

    @action(methods=['post'], detail=True)
    def remove_favorite(self, request, pk=None):
        webook_user = WeBookUser.objects.get(pk=request.user)
        webook_user.favorite_book.remove(pk)
        webook_user.save()
        return Response({"detail": "Remove Favorite Book Success."}, status=200)

    # 1. get list of possible image
    # 2. get each image
    # 3. post new image
    # 4. delete image
    @action(methods=['get', 'post'], detail=True, permission_classes=[permissions.AllowAny])
    def image(self, request, pk=None):
        book = Book.objects.get(pk=pk)
        if request.method == 'GET':
            pass
        elif request.method == 'POST':
            pass

    @action(methods=['get'], detail=False, permission_classes=[permissions.AllowAny])
    def shipping_price_datadict(self, request, format=None):
        return Response(PRICE_DICT, status=200)

class ImagePoolViewSet(viewsets.ModelViewSet):
    queryset = ImagePool.objects.all()
    serializer_class = ImagePoolSerializer
    permission_classes = (permissions.IsAuthenticated, IsObjectOwnerOrReadOnly)

    @action(methods=['get'], detail=True, permission_classes=[permissions.AllowAny])
    def get_image(self, request, pk=None):
        image_pool = ImagePool.objects.get(pk=pk)
        response = HttpResponse(image_pool.image, content_type="image/jpeg")
        return response

class BookReviewViewSet(viewsets.ModelViewSet):
    queryset = BookReview.objects.all()
    serializer_class = BookReviewSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter,)
    filterset_fields = ('book', )
    ordering_fields = ('create_dt', 'rating')
    ordering = ('-create_dt', )

class PostHasImageFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        if request.query_params.get('has_image', None) :
            return queryset.filter(~(Q(image='')|Q(image=None)))
        else :
            return queryset

class PostByUserFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        user_id = request.query_params.get('post_by', None)
        if user_id :
            user = get_user_model().objects.get(pk=user_id)
            return queryset.filter(user=user)
        else :
            return queryset

class PostByFriendFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        friend_of =request.query_params.get('friend', None)
        if friend_of :
            user = get_user_model().objects.get(pk=friend_of)
            webook_user = WeBookUser.objects.get(pk=user)
            return queryset.filter(user__in=webook_user.friend.all().values_list('user', flat=True))
        else :
            return queryset

class PostAnnouncementLocationFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        webook_user = WeBookUser.objects.get(pk=request.user)
        user_lat = float(webook_user.current_lat)
        user_long = float(webook_user.current_long)
        print(user_lat, user_long)
        queryset = queryset.exclude(Q(post_exposure='announcement')&Q(geo_lat__gte=user_lat+3)&Q(is_geo_constrain=True))
        queryset = queryset.exclude(Q(post_exposure='announcement')&Q(geo_lat__lte=user_lat-3)&Q(is_geo_constrain=True))
        queryset = queryset.exclude(Q(post_exposure='announcement')&Q(geo_long__gte=user_long+0.8)&Q(is_geo_constrain=True))
        queryset = queryset.exclude(Q(post_exposure='announcement')&Q(geo_long__lte=user_long-0.8)&Q(is_geo_constrain=True))
        return queryset

class DebugPostViewset(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend)
    filterset_fields = ('post_exposure', 'is_active')
    ordering_fields = ('create_dt', )
    ordering = ('-create_dt', )

class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.filter(is_active=True)
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend, PostHasImageFilterBackend,
                PostByUserFilterBackend, PostByFriendFilterBackend, PostAnnouncementLocationFilterBackend,
                filters.SearchFilter)
    filterset_fields = ('post_exposure', 'is_active')
    ordering_fields = ('create_dt', )
    ordering = ('-create_dt', )
    search_fields = ('title', 'post_text')

    def list(self, request):
        print("Post list(self, request) called!")
        one_day_late = datetime.datetime.now() - datetime.timedelta(days=1)
        two_day_late = datetime.datetime.now() - datetime.timedelta(days=2)
        active_announcement = Post.objects.filter(post_exposure='announcement', is_active=True, create_dt__lte=one_day_late)
        active_announce_donate = Post.objects.filter(post_exposure='announcement_donate', is_active=True, create_dt__lte=two_day_late)
        active_announcement.update(is_active=False)
        active_announce_donate.update(is_active=False)
        return super().list(request)

    # ToDo : Limit Photo size upload
    @action(methods=['post', 'get', 'delete'], detail=True, permission_classes=[permissions.AllowAny])
    def image(self, request, pk=None):
        post = get_object_or_404(self.queryset, pk=pk)
        if request.method == 'GET':
            if post.image :
                response = HttpResponse(post.image, content_type="image/jpeg")
                return response
            else :
                return Response({"detail": "No Photo."}, status=200)
        elif request.method == 'POST':
            image = request.FILES['image']
            post.image = image
            image.save()
            return Response({"detail": "Upload Success."}, status=200)
        elif request.method == 'DELETE':
            post.image.delete(save=True)
            return Response({"detail": "Delete Success."}, status=200)

    def create(self, request):
        response = super().create(request)
        webook_user = WeBookUser.objects.get(pk=request.user)
        if response.data['post_exposure'] == 'announcement' :
            if webook_user.announcement_credit > 0 :
                webook_user.announcement_credit -= 1
                d = {
                    'post_id' : response.data['id'],
                    'post_title' : response.data['title'],
                    'fullname' : webook_user.fullname,
                    'timestamp' : datetime.datetime.now().isoformat(),
                    'noti_type' : 'announcement',
                    'image' : response.data['image']
                }
                webook_user.save()
                push_gears.push_all("Post Announcement", data=d)
            else :
                return Response({"detail": "Not enough credit."}, status=400)
        elif response.data['post_exposure'] == 'announcement_donate':
            if webook_user.announcement_donate_credit > 0 :
                webook_user.announcement_donate_credit -= 1
                d = {
                    'post_id' : response.data['id'],
                    'post_title' : response.data['title'],
                    'fullname' : webook_user.fullname,
                    'timestamp' : datetime.datetime.now().isoformat(),
                    'noti_type' : 'announcement_donate',
                    'image' : response.data['image']
                }
                webook_user.save()
                push_gears.push_all("Post Announcement", data=d)
            else :
                return Response({"detail": "Not enough credit."}, status=400)
        return response

class PostCommentViewSet(viewsets.ModelViewSet):
    queryset = PostComment.objects.all()
    serializer_class = PostCommentSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_fields = ('post', )
    ordering_fields = ('create_dt', )
    ordering = ('-create_dt', )

    def create(self, request):
        response = super().create(request)
        webook_user = WeBookUser.objects.get(pk=request.user)
        post_comment = PostComment.objects.get(pk=response.data['id'])
        d = {
            'post_id' : post_comment.post.id,
            'fullname' : webook_user.fullname,
            'timestamp' : datetime.datetime.now().isoformat(),
            'noti_type' : 'post_comment',
        }
        if post_comment.post.user.id != request.user.id :
            push_gears.push_target(post_comment.post.user.id, "Post Comment", data=d, badge_count=True)
        return response

class PostLikeViewSet(mixins.CreateModelMixin, mixins.ListModelMixin,
                    mixins.RetrieveModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    queryset = PostLike.objects.all()
    serializer_class = PostLikeSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_fields = ('user', 'post')
    ordering_fields = ('-create_dt', )

    def create(self, request):
        response = super().create(request)
        webook_user = WeBookUser.objects.get(pk=request.user)
        post_like = PostLike.objects.get(pk=response.data['id'])
        d = {
            'post_id' : post_like.post.id,
            'fullname' : webook_user.fullname,
            'timestamp' : datetime.datetime.now().isoformat(),
            'noti_type' : 'post_like',
        }
        if post_like.post.user.id != request.user.id :
            push_gears.push_target(post_like.post.user.id, "Post Like", data=d, badge_count=True)
        return response

class PostNotificationViewSet(mixins.CreateModelMixin, mixins.ListModelMixin,
                    mixins.RetrieveModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    queryset = PostNotification.objects.all()
    serializer_class = PostNotificationSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_fields = ('user', 'post')
    ordering_fields = ('create_dt', )
    ordering = ('-create_dt', )

class ReportViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)

class PurchaseOrderItemViewSet(mixins.UpdateModelMixin, mixins.ListModelMixin,
                    mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = PurchaseOrderItem.objects.all()
    serializer_class = PurchaseOrderItemSerializer
    permission_classes = (permissions.IsAuthenticated, )
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_fields = ('purchase_order', 'shipment_status' )
    ordering_fields = ('create_dt', )
    ordering = ('-create_dt', )

    @action(methods=['post'], detail=True)
    def confirm_package_received(self, request, pk=None):
        po_item = get_object_or_404(self.queryset, pk=pk)
        if po_item.shipment_status == 'shipping' :
            po_item.shipment_status = 'completed'
            po_item.save()
            return Response({"detail": "Shipment confirmed."}, status=200)
        else :
            return Response({"detail": "Shipment isn't ready for confirm."}, status=400)

    @action(methods=['get'], detail=False)
    def sold_list(self, request, format=None):
        # ToDo : filter by status
        shipment_status = request.query_params.get('shipment_status', None)
        query_po_item = PurchaseOrderItem.objects.filter(item__user=request.user)
        if shipment_status :
            query_po_item = query_po_item.filter(shipment_status=shipment_status)
        po_item_s = PurchaseOrderItemSerializer(query_po_item, many=True)
        return Response( po_item_s.data, status=200 )
# PurchaseOrder status dict = not_paid, shipping, completed, cancel

class PurchaseOrderViewSet(viewsets.ModelViewSet):
    queryset = PurchaseOrder.objects.all()
    serializer_class = PurchaseOrderSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_fields = ('user', 'status')
    ordering_fields = ('create_dt', )
    ordering = ('-create_dt', )

    # ToDo : Just for testing remove after finish real paid method
    @action(methods=['get'], detail=True)
    def paid(self, request, pk=None):
        purchase_order = get_object_or_404(self.queryset, pk=pk)
        purchase_order.paid()
        return Response({"detail": "Paid success."}, status=200)

    @action(methods=['post'], detail=True)
    def coin_paid(self, request, pk=None):
        import math
        webook_user = WeBookUser.objects.get(pk=request.user)
        purchase_order = get_object_or_404(self.queryset, pk=pk)
        used_credit = math.ceil(purchase_order.total_amount)
        if webook_user.shipping_credit >= used_credit :
            webook_user.shipping_credit -= used_credit
            webook_user.save()
            purchase_order.paid("coin_paid")
            return Response({"detail": "Paid success."}, status=200)
        else :
            return Response({"detail": "Not enough shipping credit."}, status=400)

    @action(methods=['post'], detail=False)
    def create_po(self, request, format=None):
        user = request.user
        data = request.POST
        po_item = json.loads(data['po_item'])
        if po_item is None :
            return Response({"detail": "Item not provided."}, status=400)
        else :
            query_user_po = PurchaseOrder.objects.filter(user=user, status="not_paid")
            po_book_id = [ i['item'] for i in po_item]
            query_book_in_po = PurchaseOrderItem.objects.filter(item_id__in=po_book_id, purchase_order__status="not_paid")
            if query_user_po :
                return Response({"detail": "Purchase Order already created, please finish before create new one."}, status=400)
            elif query_book_in_po :
                return Response({"detail": "Book already purchased, please select new one."}, status=400)
            else :
                po = PurchaseOrder()
                po.user = user
                po.payment = data['payment']
                po.status = data['status']
                po.shipping_address = data['shipping_address']
                po.save()
                for i in po_item :
                    po_item = PurchaseOrderItem(purchase_order=po, price=float(i['price']))
                    provider = ShippingProvider.objects.get(name=i['shipping_provider'])
                    po_item.save()
                    po_item.shipping_provider = provider
                    po_item.item_id = i['item']
                    po_item.save()
                s = PurchaseOrderSerializer(po)
                return Response(s.data, status=201)
