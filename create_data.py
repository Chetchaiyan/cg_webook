from main_app.models import *

def create_data():
    query_sp = ShippingProvider.objects.all()
    if len(query_sp) != 0 :
        print("Data existed, please clear database before proceed")
        return
    shipping_provider = ShippingProvider(name='ThailandPost')
    shipping_provider.save()

create_data()
